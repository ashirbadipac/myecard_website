<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Website_model extends MY_Model
{
    public $table = 'website';
    public $timestamps = false;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Fetch data from any table based on different conditions
     *
     * @access	public
     * @param	string
     * @param	string
     * @param	array
     * @return	bool
     */
    public function fetch_data($table, $fields = '*', $conditions = array(), $returnRow = false, $distinct = false)
    {

        //Preparing query
        $this->db->select($fields);
        $this->db->from($table);

        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }

        if ($distinct) {
            $this->db->distinct();
        }

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;

        //Return
        if (FALSE != $query && $query->num_rows() > 0) {

            // $this->totalrows = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
            return $returnRow ? $query->row_array() : $query->result_array();
        } else {
            return NULL;
        }
    }

    /**
     * Count all records
     *
     * @access	public
     * @param	string
     * @return	array
     */
    public function fetch_count($table, $conditions = array())
    {
        $this->db->from($table);
        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }
        return $this->db->count_all_results();
    }

    /**
     * Handle different conditions of query
     *
     * @access	public
     * @param	array
     * @return	bool
     */
    private function condition_handler($conditions)
    {
        //Inner Join
        if (array_key_exists('inner_join', $conditions)) {

            //Iterate all where's
            foreach ($conditions['inner_join'] as $key => $val) {
                $this->db->join($key, $val, 'inner');
            }
        }

        //Left Join
        if (array_key_exists('left_join', $conditions)) {

            //Iterate all where's
            foreach ($conditions['left_join'] as $key => $val) {
                $this->db->join($key, $val, 'left');
            }
        }

        //Where
        if (array_key_exists('where', $conditions)) {

            //Iterate all where's
            foreach ($conditions['where'] as $key => $val) {
                $this->db->where($key, $val);
            }
        }

        //Where OR
        if (array_key_exists('or_where', $conditions)) {

            //Iterate all where or's
            foreach ($conditions['or_where'] as $key => $val) {
                $this->db->or_where($key, $val);
            }
        }

        //Where In
        if (array_key_exists('where_in', $conditions)) {

            //Iterate all where in's
            foreach ($conditions['where_in'] as $key => $val) {
                $this->db->where_in($key, $val);
            }
        }

        //Where Not In
        if (array_key_exists('where_not_in', $conditions)) {

            //Iterate all where in's
            foreach ($conditions['where_not_in'] as $key => $val) {
                $this->db->where_not_in($key, $val);
            }
        }

        //Having
        if (array_key_exists('having', $conditions)) {
            $this->db->having($conditions['having']);
        }

        //Group By
        if (array_key_exists('group_by', $conditions)) {
            $this->db->group_by($conditions['group_by']);
        }

        //Order By
        if (array_key_exists('order_by', $conditions)) {

            //Iterate all order by's
            foreach ($conditions['order_by'] as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        //Order By
        if (array_key_exists('like', $conditions)) {

            //Iterate all likes
            foreach ($conditions['like'] as $key => $val) {
                $this->db->like($key, $val);
            }
        }

        //Limit
        if (array_key_exists('limit', $conditions)) {

            //If offset is there too?
            if (count($conditions['limit']) == 1) {
                $this->db->limit($conditions['limit'][0]);
            } else {
                $this->db->limit($conditions['limit'][0], $conditions['limit'][1]);
            }
        }
    }

    /**
     * Insert data in DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	string
     * @return	string
     */
    public function insert_single($table, $data = array())
    {
        //Check if any data to insert
        if (count($data) < 1) {
            return false;
        }

        $this->db->insert($table, $data);
        //echo $this->db->last_query();

        return $this->db->insert_id();
    }

    public function insert_multiple($table, $data = array())
    {
        //Check if any data to insert
        if (count($data) < 1) {
            return false;
        }
        $this->db->insert_batch($table, $data);
        return true;
    }



    /**
     * Insert batch data
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	array
     * @param	bool
     * @return	bool
     */
    public function insert_batch($table, $defaultArray, $dynamicArray = array(), $updatedTime = false)
    {
        //Check if default array has values
        if (count($dynamicArray) < 1) {
            return false;
        }

        //If updatedTime is true
        if ($updatedTime) {
            $defaultArray['UpdatedTime'] = time();
        }

        //Iterate it
        foreach ($dynamicArray as $val) {
            $updates[] = array_merge($defaultArray, $val);
        }
        return $this->db->insert_batch($table, $updates);
    }

    /**
     * Update details in DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	array
     * @return	string
     */
    public function update_single($table, $updates, $conditions = array())
    {
        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }

        return $this->db->update($table, $updates);
        // echo $this->db->last_query(); exit;
    }

    public function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) {
            return $min;
        }
        // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    public function getRandomString($length)
    {
        $token = "";
        $codeAlphabet = "abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $max = strlen($codeAlphabet);
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max - 1)];
        }
        return $token;
    }
}
