<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Common_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public $secret_key = '847EJDALQIS2PE3UDKA7128409EJA';
    public function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) {
            return $min;
        }
        // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    public function getOTPText($length)
    {
        $token = "";
        $codeAlphabet = "123456789";
        $max = strlen($codeAlphabet);
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max - 1)];
        }
        return $token;
    }

    public function getReferral($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $max = strlen($codeAlphabet);
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max - 1)];
        }
        return $token;
    }

    function encrypt_decrypt($action, $string, $secret_key = "")
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_iv = 'ipac@12361'; // change this to one more secure
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {

            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    public function sendSMS($phone_no, $msg, $tid, $unicode = false)
    {
        $encsmsmsg = urlencode($msg);
        $accessKey = 'KsH7ZeqMnsTTPCldFRVP9525P4sFvB';
        $msisdn = trim($phone_no);
        $from = 'TMCMGL';
        if ($unicode) {
            $getUrl = "https://api.mobilnxt.in/api/push?accesskey=$accessKey&to=$msisdn&text=$encsmsmsg&from=$from&tid=$tid&unicode=1";
        } else {
            $getUrl = "https://api.mobilnxt.in/api/push?accesskey=$accessKey&to=$msisdn&text=$encsmsmsg&from=$from&tid=$tid";
        }

       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $getUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $res = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $responseBody = json_decode($res);
        curl_close($ch);
    }


    function sendIVR($IVRUrl)
    {
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $IVRUrl);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 300);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
        $res2 = curl_exec($ch2);
        $httpCode2 = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);
    }

    public function sendEmail($email, $subject, $mesg)
    {
        /** Email Setup */
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://ipac.splitmx.com',
            'smtp_port' => 465,
            'smtp_user' => 'info@campaign4india.com',
            'smtp_pass' => '_mkN574_7XkZ;Dfs',
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        // Set to, from, message, etc.
        $this->email->from('info@goenchinavisakal.com', 'Goenchi Navi Sakal');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($mesg);
        $this->email->send();
    }
}
