<?php

defined('BASEPATH') or exit('No direct script access allowed');

class YSCWebsite_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Fetch data from any table based on different conditions
     *
     * @access	public
     * @param	string
     * @param	string
     * @param	array
     * @return	bool
     */
    public function fetch_data($table, $fields = '*', $conditions = array(), $returnRow = false, $distinct = false)
    {
        $ysc_db = $this->load->database('ysc_db', TRUE);

        //Preparing query
        $ysc_db->select($fields);
        $ysc_db->from($table);

        //If there are conditions
        if (count($conditions) > 0) {
            //Inner Join
            if (array_key_exists('inner_join', $conditions)) {

                //Iterate all where's
                foreach ($conditions['inner_join'] as $key => $val) {
                    $ysc_db->join($key, $val, 'inner');
                }
            }

            //Left Join
            if (array_key_exists('left_join', $conditions)) {

                //Iterate all where's
                foreach ($conditions['left_join'] as $key => $val) {
                    $ysc_db->join($key, $val, 'left');
                }
            }

            //Where
            if (array_key_exists('where', $conditions)) {

                //Iterate all where's
                foreach ($conditions['where'] as $key => $val) {
                    $ysc_db->where($key, $val);
                }
            }

            //Where OR
            if (array_key_exists('or_where', $conditions)) {

                //Iterate all where or's
                foreach ($conditions['or_where'] as $key => $val) {
                    $ysc_db->or_where($key, $val);
                }
            }

            //Where In
            if (array_key_exists('where_in', $conditions)) {

                //Iterate all where in's
                foreach ($conditions['where_in'] as $key => $val) {
                    $ysc_db->where_in($key, $val);
                }
            }

            //Where Not In
            if (array_key_exists('where_not_in', $conditions)) {

                //Iterate all where in's
                foreach ($conditions['where_not_in'] as $key => $val) {
                    $ysc_db->where_not_in($key, $val);
                }
            }

            //Having
            if (array_key_exists('having', $conditions)) {
                $ysc_db->having($conditions['having']);
            }

            //Group By
            if (array_key_exists('group_by', $conditions)) {
                $ysc_db->group_by($conditions['group_by']);
            }

            //Order By
            if (array_key_exists('order_by', $conditions)) {

                //Iterate all order by's
                foreach ($conditions['order_by'] as $key => $val) {
                    $ysc_db->order_by($key, $val);
                }
            }

            //Order By
            if (array_key_exists('like', $conditions)) {

                //Iterate all likes
                foreach ($conditions['like'] as $key => $val) {
                    $ysc_db->like($key, $val);
                }
            }

            //Limit
            if (array_key_exists('limit', $conditions)) {

                //If offset is there too?
                if (count($conditions['limit']) == 1) {
                    $ysc_db->limit($conditions['limit'][0]);
                } else {
                    $ysc_db->limit($conditions['limit'][0], $conditions['limit'][1]);
                }
            }
        }

        if ($distinct) {
            $ysc_db->distinct();
        }

        $query = $ysc_db->get();

        //Return
        if (FALSE != $query && $query->num_rows() > 0) {

            // $this->totalrows = $ysc_db->query('SELECT FOUND_ROWS() count;')->row()->count;
            return $returnRow ? $query->row_array() : $query->result_array();
        } else {
            return NULL;
        }
    }

    /**
     * Insert data in DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	string
     * @return	string
     */
    public function insert_single($table, $data = array())
    {
        $ysc_db = $this->load->database('ysc_db', TRUE);
        //Check if any data to insert
        if (count($data) < 1) {
            return false;
        }

        $ysc_db->insert($table, $data);
        //echo $ysc_db->last_query();

        return $ysc_db->insert_id();
    }


    /**
     * Update details in DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	array
     * @return	string
     */
    public function update_single($table, $updates, $conditions = array())
    {
        $ysc_db = $this->load->database('ysc_db', TRUE);
        //If there are conditions
        if (count($conditions) > 0) {
            //Inner Join
            if (array_key_exists('inner_join', $conditions)) {

                //Iterate all where's
                foreach ($conditions['inner_join'] as $key => $val) {
                    $ysc_db->join($key, $val, 'inner');
                }
            }

            //Left Join
            if (array_key_exists('left_join', $conditions)) {

                //Iterate all where's
                foreach ($conditions['left_join'] as $key => $val) {
                    $ysc_db->join($key, $val, 'left');
                }
            }

            //Where
            if (array_key_exists('where', $conditions)) {

                //Iterate all where's
                foreach ($conditions['where'] as $key => $val) {
                    $ysc_db->where($key, $val);
                }
            }

            //Where OR
            if (array_key_exists('or_where', $conditions)) {

                //Iterate all where or's
                foreach ($conditions['or_where'] as $key => $val) {
                    $ysc_db->or_where($key, $val);
                }
            }

            //Where In
            if (array_key_exists('where_in', $conditions)) {

                //Iterate all where in's
                foreach ($conditions['where_in'] as $key => $val) {
                    $ysc_db->where_in($key, $val);
                }
            }

            //Where Not In
            if (array_key_exists('where_not_in', $conditions)) {

                //Iterate all where in's
                foreach ($conditions['where_not_in'] as $key => $val) {
                    $ysc_db->where_not_in($key, $val);
                }
            }

            //Having
            if (array_key_exists('having', $conditions)) {
                $ysc_db->having($conditions['having']);
            }

            //Group By
            if (array_key_exists('group_by', $conditions)) {
                $ysc_db->group_by($conditions['group_by']);
            }

            //Order By
            if (array_key_exists('order_by', $conditions)) {

                //Iterate all order by's
                foreach ($conditions['order_by'] as $key => $val) {
                    $ysc_db->order_by($key, $val);
                }
            }

            //Order By
            if (array_key_exists('like', $conditions)) {

                //Iterate all likes
                foreach ($conditions['like'] as $key => $val) {
                    $ysc_db->like($key, $val);
                }
            }

            //Limit
            if (array_key_exists('limit', $conditions)) {

                //If offset is there too?
                if (count($conditions['limit']) == 1) {
                    $ysc_db->limit($conditions['limit'][0]);
                } else {
                    $ysc_db->limit($conditions['limit'][0], $conditions['limit'][1]);
                }
            }
        }
        return $ysc_db->update($table, $updates);
        // echo $ysc_db->last_query(); exit;
    }
}
