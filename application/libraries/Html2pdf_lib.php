<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'libraries/html2pdf/vendor/autoload.php');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Html2pdf_lib
{
  function __construct($params = null)
  {
    $this->html2pdf = new Html2Pdf('P', 'A4', 'en', true, 'UTF-8', 0);
    $this->CI = &get_instance();
  }

  function converHtml2pdf($content, $filename = null)
  {
    try {
      $this->html2pdf->setTestIsImage(false);
      $this->html2pdf->pdf->SetDisplayMode('fullpage');
      $this->html2pdf->writeHTML($content);
      $this->html2pdf->output($filename);
    } catch (Html2PdfException $e) {
      $this->html2pdf->clean();

      $formatter = new ExceptionFormatter($e);
      echo $formatter->getHtmlMessage();
    }
  }
}
