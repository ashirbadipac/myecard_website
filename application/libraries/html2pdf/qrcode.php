<?php
/**
 * Html2Pdf Library - example
 *
 * HTML => PDF converter
 * distributed under the OSL-3.0 License
 *
 * @package   Html2pdf
 * @author    Laurent MINGUET <webmaster@html2pdf.fr>
 * @copyright 2017 Laurent MINGUET
 */

require_once dirname(__FILE__).'/vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

try {
    $content = '<page backtop="10mm">
    <qrcode value="Pradip Nandwana" ec="L" style="width: 30mm;"></qrcode>
    <qrcode value="Pradip Nandwana" ec="M" style="width: 30mm;"></qrcode>
    <qrcode value="Pradip Nandwana" ec="Q" style="width: 30mm;"></qrcode>
    <qrcode value="Pradip Nandwana" ec="H" style="width: 30mm;"></qrcode>
    <br>
    <qrcode value="Pradip Nandwana" style="width: 20mm;"></qrcode>
    <qrcode value="Pradip Nandwana" style="width: 30mm;"></qrcode>
    <qrcode value="Pradip Nandwana" style="width: 40mm;"></qrcode>
    <qrcode value="Pradip Nandwana" style="width: 50mm;"></qrcode>
    <br>
    
    <qrcode value="Pradip Nandwana" style="width: 40mm; background-color: white; color: black;"></qrcode>
    <qrcode value="Pradip Nandwana" style="width: 40mm; background-color: yellow; color: red"></qrcode>
    <qrcode value="Pradip Nandwana" style="width: 40mm; background-color: #FFCCFF; color: #003300"></qrcode>
    <qrcode value="Pradip Nandwana" style="width: 40mm; background-color: #CCFFFF; color: #003333"></qrcode>
    <br>
</page>';

    $html2pdf = new Html2Pdf('P', 'A4', 'fr');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->writeHTML($content);
    $html2pdf->output('qrcode.pdf');
} catch (Html2PdfException $e) {
    $html2pdf->clean();

    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}
