<?php
defined('BASEPATH') or exit('No direct script access allowed');
$current_lang = $_SESSION['set_language'];
$default_lang = '';
if ($current_lang != 'en') {
    $default_lang = 'd-lang';
}
?>

<!--   Core JS Files   -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<?php
if ($current_lang == 'en') {?>
    <script src="<?php echo base_url(); ?>assets/js/lang/english_lang.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js?v=2.2"></script>
<?php } else if ($current_lang == 'kha') {?>
    <script src="<?php echo base_url(); ?>assets/js/lang/khasi_lang.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js?v=2.2"></script>
<?php } else {?>
    <script src="<?php echo base_url(); ?>assets/js/lang/garo_lang.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js?v=2.2"></script>
<?php }?>
<script src="<?php echo base_url(); ?>assets/js/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $(".monthDOB").on("change", function() {
            var month = jQuery(this).val();
            monthChange(month);
        });
        dayDOBchange();
        $("#district").change(function() {
            if ($("#district").val() != "") {
                getAC();
                
            } else {
                $("#ac").empty();
                $("<option>").val("").text('<?php echo $this->lang->line('select_ac'); ?>').appendTo($("#ac"));

               
            }
        });
    });
    function getAC() {
        ajaxindicatorstart("Please Wait..");
        var dataString = "district=" + $("#district").val();
        $.ajax({
            type: "POST",
            data: dataString,
            url: "<?php echo base_url(); ?>welcome/getAC",
            cache: false,
            dataType: "json",
            success: function(html) {
                ajaxindicatorstop();
                $("#ac").empty();
                $("<option>")
                    .val("")
                    .text("<?php echo $this->lang->line('select_ac'); ?>")
                    .appendTo($("#ac"));
                $.each(html, function(key, value) {
                    var ac_name = value.ac;
                    $("<option>")
                        .val(ac_name)
                        .text(ac_name)
                        .attr("data-params", value.ac_code)
                        .appendTo($("#ac"));
                        
                });
                
            },
        });
    }

    function monthChange(month, day = "") {
        var addOption = "";
        $(".dayDOB").empty();
        addOption = addOption + '<option value=""><?php echo $this->lang->line('dob_day'); ?></option>';
        if (month != "") {
            if (month === "2") {
                dayCount = 29;
            }
            if (month === "4" || month === "6" || month === "9" || month === "11") {
                dayCount = 30;
            }
            if (
                month === "1" ||
                month === "3" ||
                month === "5" ||
                month === "7" ||
                month === "8" ||
                month === "10" ||
                month === "12"
            ) {
                dayCount = 31;
            }
            for (var i = 1; i <= dayCount; i++) {
                var valueselect = day == i ? "selected" : "";
                addOption =
                    addOption +
                    "<option value=" +
                    i +
                    " " +
                    valueselect +
                    ">" +
                    i +
                    "</option>";
            }
            $(".dayDOB").append(addOption);
        }
    }

    function dayDOBchange(y = "") {
        var addOptionYear = "";
        var year = 2005;
        for (var i = 1; i <= 100; i++) {
            year--;
            var valueselect = y == year ? "selected" : "";
            addOptionYear =
                addOptionYear +
                "<option value=" +
                year +
                " " +
                valueselect +
                ">" +
                year +
                "</option>";
        }
        $(".yearDOB").append(addOptionYear);
    }
</script>

</body>

</html>