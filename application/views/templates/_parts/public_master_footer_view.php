<?php
defined('BASEPATH') or exit('No direct script access allowed');
$current_lang = $_SESSION['set_language'];
$default_lang = '';
if ($current_lang != 'en') {
    $default_lang = 'd-lang';
}
?>

<!--   Core JS Files   -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('.counter-count').each(function() {
      $(this).prop('Counter', 0).animate({
        Counter: $(this).text()
      }, {
        duration: 5000,
        easing: 'swing',
        step: function(now) {
          $(this).html(Math.ceil(now));
        }
      });
    });
        $('#mamata-video img').click(function() {
            var video =
                '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" allow="autoplay; encrypted-media" src="' +
                $('#mamata-video img').attr('data-video') + '"></iframe></div>';
            $('.mks_video').html(video);
            $(this).hide();
        });

        $('#mamata-video-m img').click(function() {
            var video =
                '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" allow="autoplay; encrypted-media" src="' +
                $('#mamata-video img').attr('data-video') + '"></iframe></div>';
            $('.mks_video_m').html(video);
            $(this).hide();
        });
    });

    function thankyou(msg) {
        swal("", msg, "success");
    }
</script>

</body>

</html>