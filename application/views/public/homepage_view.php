<?php
   defined('BASEPATH') or exit('No direct script access allowed');
   $current_lang = $_SESSION['set_language'];
   $default_lang = '';
   if ($current_lang != 'en') {
       $default_lang = 'd-lang';
   }
   
   $rid = '';
   if (isset($_GET['rid'])) {
       if (!empty($_GET['rid'])) {
           $rid = $_GET['rid'];
       }
   }
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>MYE Card</title>
      <meta name="google-site-verification" content="" />
      <meta name="facebook-domain-verification" content="6r3qd2iuoct750fyvxneleux4y0vk3" />
      <meta name="description" content="" />
      <meta name="keywords" content=" TMC, AITC" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta name="theme-color" content="#29a2db" />
      <meta name="msapplication-navbutton-color" content="#29a2db" />
      <meta name="apple-mobile-web-app-status-bar-style" content="#29a2db" />
      <meta name="robots" content="index, follow" />
      <meta name="format-detection" content="telephone=no">
      <link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/color.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-select.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/media_screen.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
      <style>
         @font-face {
         font-family: helveticaregular;
         src: url("<?php echo base_url(); ?>/assets/fonts/HELVETICANEUELTPRO-MDCN.OTF") format("truetype");
         }
         @font-face {
         font-family: myraidpro;
         src: url("<?php echo base_url(); ?>/assets/fonts/MYRIADPRO-REGULAR.OTF") format("truetype");
         }
         .main-about {
         background: url('<?php echo base_url(); ?>assets/images/<?php echo $current_lang ?>/banner.jpg') center bottom;
         background-size: cover;
         position: relative;
         min-height: 88vh;
         background-color: #FFFFFF;
         background-repeat: no-repeat;
         }
         @media (max-width: 821px) {
         .main-about {
         background: url('<?php echo base_url(); ?>assets/images/<?php echo $current_lang ?>/banner.jpg') center bottom;
         background-size: cover;
         position: relative;
         min-height: 40vh;
         background-color: #FFFFFF;
         background-repeat: no-repeat;
         }
         }
         @media (max-width: 767px) {
         .main-about {
         background: url('<?php echo base_url(); ?>assets/images/<?php echo $current_lang ?>/m-banner.jpg') center center;
         background-size: 100% 97%;
         position: relative;
         min-height: 80vh;
         background-color: #FFFFFF;
         background-position: bottom;
         background-repeat: no-repeat;
         }
         }
         @media (max-width: 376px) {
         .main-about {
         background: url('<?php echo base_url(); ?>assets/images/<?php echo $current_lang ?>/m-banner.jpg') center center;
         background-size: 100% 97%;
         position: relative;
         min-height: 82vh !important;
         background-color: #FFFFFF;
         background-position: bottom;
         background-repeat: no-repeat;
         }
         }
         @media (max-width: 769px) {
         .logo img {
         max-width: 45px;
         }
         .signup-div {
         bottom: 12%;
         right: 0%;
         }
         }
      </style>
      <meta name="facebook-domain-verification" content="ed2kh007ps09shcrrmi6ewl24kkvzm" />
      <!-- Google tag (gtag.js) -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-PWY2JTVZB5"></script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         
         gtag('config', 'G-PWY2JTVZB5');
      </script>
      <!-- Facebook Pixel Code -->
      <script>
         !function(f,b,e,v,n,t,s)
         {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};
         if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
         n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];
         s.parentNode.insertBefore(t,s)}(window, document,'script',
         'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '1980244685612613');
         fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=1980244685612613&ev=PageView&noscript=1"
         /></noscript>
      <!-- End Facebook Pixel Code -->  
      <!-- Event snippet for TMC My E Card Sign Up Manually conversion page -->
      <script>
         gtag('event', 'conversion', {'send_to': 'AW-722692676/4LkACJiV3YYYEMTUzdgC'});
      </script>
      <!-- Event snippet for MYE Card Sign-up conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-11059296387/oSwMCLmEnocYEIPxvZkp'});
</script>

   </head>
   <body>
      <!--loader-->
      <div class="loader-wrap">
         <div class="loader-inner">
            <div class="loader-inner-cirle">
            </div>
         </div>
      </div>
      <!--loader end-->
      <div id="main">
         <div id="wrapper">
            <div class="content">
               <!-- Start: Header Section -->
               <div class="main-about">
                  <div class="header_banner">
                     <div class="row justify-content-start  custom-padding ">
                        <div class="col-12 p-0">
                           <div class="header-inner d-flex flex-row justify-content-end justify-content-md-end align-items-center py-2">
                              <div class="lang-wrap">
                                 <div class="show-lang">
                                    <span>
                                    <i class="fa fa-globe"></i>
                                    <strong>
                                    <?php echo $current_lang == 'en' ? $this->lang->line('lang_english') : ($current_lang == 'kha' ? $this->lang->line('lang_khasi') : $this->lang->line('lang_garo')); ?>
                                    </strong>
                                    </span>
                                 </div>
                                 <ul class="lang-tooltip no-list-style">
                                    <?php
                                       $string = '';
                                       $string .=  $rid != '' ? '?rid=' . $rid : '';
                                       ?>
                                    <li><a style="text-decoration:none" href="<?php echo base_url() . $string ?>" class="<?php echo $current_lang == 'en' ? 'current-lan' : '' ?>"><?php echo $this->lang->line('lang_english'); ?></a></li>
                                    <li><a style="text-decoration:none" href="<?php echo base_url() . 'garo' . $string; ?>" class="<?php echo $current_lang == 'garo' ? 'current-lan' : '' ?>"><?php echo $this->lang->line('lang_garo'); ?></a></li>
                                    <li><a style="text-decoration:none" href="<?php echo base_url() . 'kha' . $string; ?>" class="<?php echo $current_lang == 'kha' ? 'current-lan' : '' ?>"><?php echo $this->lang->line('lang_khasi'); ?></a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="signup-div">
                     <div class="container">
                        <div class="row sgnUpBtn">
                           <div class="col-6 col-6 ml-lg-auto pl-md-0 pl-0">
                              <a href="<?php echo $this->lang->line('signup_url') . $string ?>"><img src="<?php echo base_url(); ?>assets/images/<?php echo $current_lang ?>/btn.png" style="max-width:180px;"></a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="desktop-slider" class="carousel slide d-none" data-ride="carousel">
                     <img class="w-100 desktop" src="<?php echo base_url(); ?>assets/images/<?php echo $current_lang ?>/banner.jpg">
                     <img class="w-100 mobile" src="<?php echo base_url(); ?>assets/images/<?php echo $current_lang ?>/m-banner.jpg">
                  </div>
               </div>
               <!-- End: Header Section -->
               <!-- Counter Section -->
               <section class="py-2 desktop">
                  <div class="row d-flex justify-content-center align-items-center counter ">
                     <div class="col-12 text-center d-flex justify-content-around align-items-center  ">
                        <h3 class="mb-0 mr-1">
                           <span class="counter-count"><?php echo $data['total_supporter'] ?></span> +
                        </h3>
                        <p><?php echo $this->lang->line('total_support'); ?></p>
                     </div>
                  </div>
               </section>
               <section class="py-1 mobile">
                  <div class="row d-flex justify-content-center align-items-center counter p-0">
                     <div class="col-12">
                        <h3 class="mb-0 mr-1">
                           <span class="counter-count"><?php echo $data['total_supporter'] ?></span> +
                        </h3>
                     </div>
                     <div class="col-12">
                        <p><?php echo $this->lang->line('total_support'); ?></p>
                     </div>
                  </div>
               </section>
               <!-- End:: Counter Section -->
               <!-- Start: About Section -->
               <div class="about">
                  <section>
                     <div class="container">
                        <div class="row justify-content-center">
                           <div class="col-md-12 col-12">
                              <h2> <?php echo $this->lang->line('about'); ?></h2>
                           </div>
                           <!-- <div class="col-md-6 mb-2 ">
                              <div id="mamata-video" class="about-us">
                                  <?php $front =  $current_lang == 'en' ? 'landing-video.jpg' : ($current_lang == 'kok' ? 'landing-video-kok.jpg' : 'landing-video-mr.jpg'); ?>
                                  <img src="<?php echo base_url(); ?>assets/images/<?php echo $front; ?>" alt="Join TMC" data-video="<?php echo $current_lang == 'en' ? $data['landing_video'] : ($current_lang == 'kok' ? $data['landing_video_kok'] : $data['landing_video_mr']) ?>">
                                  <div class="mks_video"></div>
                              </div>
                              </div> -->
                           <div class="col-md-12 d-flex align-items-center">
                              <p class="text-center"><?php echo $this->lang->line('about_text'); ?></p>
                           </div>
                        </div>
                     </div>
                  </section>
               </div>
               <!-- End: About Section -->
               <!-- Start: Faq  Section -->
               <section class="faq">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-12 col-12">
                           <h2> <?php echo $this->lang->line('faqs'); ?></h2>
                           <div class="accordion" id="faq">
                              <div class="card">
                                 <div class="card-header" id="faqhead1">
                                    <a href="#" class="btn btn-header-link" data-toggle="collapse" data-target="#faq1" aria-expanded="true" aria-controls="faq1"><?php echo $this->lang->line('faq1-q'); ?></a>
                                 </div>
                                 <div id="faq1" class="collapse show" aria-labelledby="faqhead1" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq1-a'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead2">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq2" aria-expanded="true" aria-controls="faq2"><?php echo $this->lang->line('faq2-q'); ?></a>
                                 </div>
                                 <div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq2-a-0'); ?></br>
                                       <?php echo $this->lang->line('faq2-a-1'); ?></br>
                                       <?php echo $this->lang->line('faq2-a-2'); ?></br>
                                       <?php echo $this->lang->line('faq2-a-3'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead3">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq3" aria-expanded="true" aria-controls="faq3"><?php echo $this->lang->line('faq3-q'); ?></a>
                                 </div>
                                 <div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq3-a-0'); ?></br>
                                       <?php echo $this->lang->line('faq3-a-1'); ?></br>
                                       <?php echo $this->lang->line('faq3-a-2'); ?></br>
                                       <?php echo $this->lang->line('faq3-a-3'); ?></br>
                                       <?php echo $this->lang->line('faq3-a-4'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead4">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq4" aria-expanded="true" aria-controls="faq4"><?php echo $this->lang->line('faq4-q'); ?></a>
                                 </div>
                                 <div id="faq4" class="collapse" aria-labelledby="faqhead4" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq4-a'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead5">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq5" aria-expanded="true" aria-controls="faq5"><?php echo $this->lang->line('faq5-q'); ?></a>
                                 </div>
                                 <div id="faq5" class="collapse" aria-labelledby="faqhead5" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq5-a'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead6">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq6" aria-expanded="true" aria-controls="faq6"><?php echo $this->lang->line('faq6-q'); ?></a>
                                 </div>
                                 <div id="faq6" class="collapse" aria-labelledby="faqhead6" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq6-a'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead7">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq7" aria-expanded="true" aria-controls="faq7"><?php echo $this->lang->line('faq7-q'); ?></a>
                                 </div>
                                 <div id="faq7" class="collapse" aria-labelledby="faqhead7" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq7-a'); ?></br>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead8">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq8" aria-expanded="true" aria-controls="faq8"><?php echo $this->lang->line('faq8-q'); ?></a>
                                 </div>
                                 <div id="faq8" class="collapse" aria-labelledby="faqhead8" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq8-a'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead9">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq9" aria-expanded="true" aria-controls="faq9"><?php echo $this->lang->line('faq9-q'); ?></a>
                                 </div>
                                 <div id="faq9" class="collapse" aria-labelledby="faqhead9" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq9-a'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead10">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq10" aria-expanded="true" aria-controls="faq10"><?php echo $this->lang->line('faq10-q'); ?></a>
                                 </div>
                                 <div id="faq10" class="collapse" aria-labelledby="faqhead10" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq10-a'); ?></br>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead11">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq11" aria-expanded="true" aria-controls="faq11"><?php echo $this->lang->line('faq11-q'); ?></a>
                                 </div>
                                 <div id="faq11" class="collapse" aria-labelledby="faqhead11" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq11-a'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead12">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq12" aria-expanded="true" aria-controls="faq12"><?php echo $this->lang->line('faq12-q'); ?></a>
                                 </div>
                                 <div id="faq12" class="collapse" aria-labelledby="faqhead12" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq12-a'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead13">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq13" aria-expanded="true" aria-controls="faq13"><?php echo $this->lang->line('faq13-q'); ?></a>
                                 </div>
                                 <div id="faq13" class="collapse" aria-labelledby="faqhead13" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq13-a'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead14">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq14" aria-expanded="true" aria-controls="faq14"><?php echo $this->lang->line('faq14-q'); ?></a>
                                 </div>
                                 <div id="faq14" class="collapse" aria-labelledby="faqhead14" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq14-a'); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="faqhead15">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq15" aria-expanded="true" aria-controls="faq15"><?php echo $this->lang->line('faq15-q'); ?></a>
                                 </div>
                                 <div id="faq15" class="collapse" aria-labelledby="faqhead15" data-parent="#faq">
                                    <div class="card-body">
                                       <?php echo $this->lang->line('faq15-a'); ?>
                                    </div>
                                 </div>
                              </div>
                              <!-- <div class="card">
                                 <div class="card-header" id="faqhead16">
                                     <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq16" aria-expanded="true" aria-controls="faq16"><?php echo $this->lang->line('faq16-q'); ?></a>
                                 </div>
                                 
                                 <div id="faq16" class="collapse" aria-labelledby="faqhead16" data-parent="#faq">
                                     <div class="card-body">
                                         <?php echo $this->lang->line('faq16-a'); ?>
                                     </div>
                                 </div>
                                 </div> -->
                              <!-- <div class="card">
                                 <div class="card-header" id="faqhead17">
                                     <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq17" aria-expanded="true" aria-controls="faq17"><?php echo $this->lang->line('faq17-q'); ?></a>
                                 </div>
                                 
                                 <div id="faq17" class="collapse" aria-labelledby="faqhead17" data-parent="#faq">
                                     <div class="card-body">
                                         <?php echo $this->lang->line('faq17-a'); ?>
                                     </div>
                                 </div>
                                 </div>
                                 <div class="card">
                                 <div class="card-header" id="faqhead18">
                                     <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq18" aria-expanded="true" aria-controls="faq18"><?php echo $this->lang->line('faq18-q'); ?></a>
                                 </div>
                                 
                                 <div id="faq18" class="collapse" aria-labelledby="faqhead18" data-parent="#faq">
                                     <div class="card-body">
                                         <?php echo $this->lang->line('faq18-a'); ?>
                                     </div>
                                 </div>
                                 </div>
                                 <div class="card">
                                 <div class="card-header" id="faqhead19">
                                     <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq19" aria-expanded="true" aria-controls="faq19"><?php echo $this->lang->line('faq19-q'); ?></a>
                                 </div>
                                 
                                 <div id="faq19" class="collapse" aria-labelledby="faqhead19" data-parent="#faq">
                                     <div class="card-body">
                                         <?php echo $this->lang->line('faq19-a'); ?>
                                     </div>
                                 </div>
                                 </div> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <!-- End: Faq  Section -->
            </div>
         </div>
         <footer class="main-footer fl-wrap">
            <div class="sub-footer fl-wrap">
               <div class="container">
                  <div class="copyright"><?php echo $this->lang->line('copyright'); ?></div>
                  <!-- <div class="footer-menu">
                     <ul class="no-list-style">
                         <li class="text-white social-footer">
                             <a href="<?php echo base_url(); ?>privacy-policy.html" target="_blank">Privacy Policy</a>
                             <a href="<?php echo base_url(); ?>terms-and-conditions.html" target="_blank">Terms & conditions</a>
                         </li>
                     </ul>
                     </div> -->
                  <div class="subfooter-nav">
                     <ul class="no-list-style">
                        <li class="text-white social-footer">
                           <?php echo $this->lang->line('follow_text'); ?>&nbsp;
                           <a href="https://www.facebook.com/AITC4Meghalaya" target="_blank"><i class="fab fa-facebook-f"></i></a>
                           <a href="https://twitter.com/AITC4Meghalaya" target="_blank"><i class="fab fa-twitter" target="_blank"></i></a>
                           <a href="https://www.instagram.com/aitc4meghalaya/" target="_blank"><i class="fab fa-instagram" target="_blank"></i></a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </footer>
      </div>