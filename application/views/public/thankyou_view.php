<?php
defined('BASEPATH') or exit('No direct script access allowed');
$current_lang = $_SESSION['set_language'];
$default_lang = '';
if ($current_lang != 'en') {
	$default_lang = 'd-lang';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>MYE Card</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="theme-color" content="#299ee1" />
	<meta name="msapplication-navbutton-color" content="#299ee1" />
	<meta name="apple-mobile-web-app-status-bar-style" content="#299ee1" />
	<meta name="robots" content="noindex" />
	<meta name="format-detection" content="telephone=no">
	<link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/thankyou.css?v=1.2">
	<meta name="facebook-domain-verification" content="ed2kh007ps09shcrrmi6ewl24kkvzm" />
	<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-PWY2JTVZB5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-PWY2JTVZB5');
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1980244685612613');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1980244685612613&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->  
<!-- Event snippet for TMC My E Card Sign Up Manually conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-722692676/4LkACJiV3YYYEMTUzdgC'});
</script>
<!-- Event snippet for MYE Card Sign-up conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-11059296387/oSwMCLmEnocYEIPxvZkp'});
</script>

</head>

<body class="custom-contain">

	<div class="container-fluid">

		<!-- <?php $desktop_header_banner = $current_lang == 'en' ? 'header_banner.jpg' : 'header_banner_kok.jpg'; ?>
		<?php $mobile_header_banner = $current_lang == 'en' ? 'mobile_banner.jpg' : 'mobile_banner_kok.jpg'; ?> -->
		<?php $desktop_header_banner = $current_lang == 'en' ? 'header_banner.jpg' : ($current_lang == 'kha' ? 'header_banner_kha.jpg' : 'header_banner_garo.jpg'); ?>
		<?php $mobile_header_banner = $current_lang == 'en' ? 'mobile_banner.jpg' : ($current_lang == 'kha' ? 'mobile_banner_kha.jpg' : 'header_banner_garo.jpg'); ?>
		<img class="w-100 desktop" src="<?php echo base_url(); ?>assets/images/<?php echo $desktop_header_banner; ?>" alt="MYE Card">
		<img class="w-100 mobile" src="<?php echo base_url(); ?>assets/images/<?php echo $mobile_header_banner; ?>" alt="MYE Card">
	</div>

	<section class="bg-grey">
		<div class="container">
			<div class="row justify-content-center <?php echo $default_lang ?>">
				<div class="col-md-12">
					<?php if ($data['total_members'] == 0) { ?>
						<h4 class="thank_title"><?php echo str_replace('[NAME]', $data['user']['user_name'], $this->lang->line('welcome_to_tmc')) ?></h4>
						<h5 style="color: #333a8b;font-weight:bold;"><?php echo $this->lang->line('reg_date') ?>: <?php echo date("d-m-Y H:i A", strtotime($data['user']['registration_date'])); ?></h5>
					<?php } ?>
				</div>
				<div class="row ">
					<div class="col-md-12">
						<a class="download_btn btn btn-primary" href="<?php echo base_url(); ?>assets/images/myecard_<?php echo $current_lang ?>.pdf" download><i class="fa fa-download"></i> <?php echo $this->lang->line('thank_download_welcome'); ?></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="pt-0 bg-grey">
		<div class="container m-card <?php echo $default_lang ?>">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
					<?php $front = $current_lang == 'en' ? 'front.jpg' : ($current_lang == 'kha' ? 'front_kha.jpg' : 'front_garo.jpg'); ?>
					<img class="w-100" src="<?php echo base_url(); ?>assets/images/<?php echo $front; ?>" alt="">
					<div class="row card-content">
						<div class="col-md-12">
							<p><?php echo $data['user']['card_format'] ?></p>
						</div>
					</div>
					<h6 class="mt-2 text-muted"><?php echo $this->lang->line('thank_front_view'); ?></h6>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
					<?php $back = $current_lang == 'en' ? 'back.jpg' : ($current_lang == 'kha' ? 'back_kha.jpg' : 'back_garo.jpg'); ?>
					<img class="w-100" src="<?php echo base_url(); ?>assets/images/<?php echo $back; ?>">
					<p class="qr"><?php echo $data['user']['card_pin'] ?></p>
					<h6 class="mt-2 text-muted"><?php echo $this->lang->line('thank_back_view'); ?></h6>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<a class="download_btn btn btn-primary" href="<?php echo base_url() . 'download?uid=' . $data['user']['encrypted_phone_no'] . '&lang=' . $current_lang ?>" download><i class="fa fa-download"></i> <?php echo $this->lang->line('thank_download_btn'); ?></a>
				</div>
			</div>
		</div>
	</section>
	<section class="step-section <?php echo $default_lang ?>">
		<div class="container">
			<div class="row justify-content-around d-none">
				<div class="col-5 d-flex step joinstep">
					<fieldset>
						<legend><?php echo $this->lang->line('thank_join_watsapp'); ?></legend>
						<img class="icon-img" src="<?php echo base_url(); ?>assets/images/step2-icon.png" alt="">
						<p><?php echo $this->lang->line('thank_click_to_connect'); ?></p>
						<div class="text-center">
							<a type="button" href="https://www.tmcwecard.com/joinwhatsapp/<?php echo $data['user']['ac_name'] ?>" target="_blank" class="btn btn-primary whatsapp_btn"><?php echo $this->lang->line('thank_join_btn'); ?></a>
						</div>
					</fieldset>
				</div>
			</div>

		</div>
	</section>

	<footer class="main-footer fl-wrap">
		<img class="w-100" src="<?php echo base_url(); ?>assets/images/footer_<?php echo $current_lang ?>.jpg" alt="WE Card">
		<div class="sub-footer fl-wrap">
			<div class="container">
				<div class="copyright"><?php echo $this->lang->line('copyright'); ?></div>
				<div class="subfooter-nav">
					<ul class="no-list-style">
						<li class="text-white social-footer">
							<?php echo $this->lang->line('follow_text'); ?>&nbsp;
							<a href="https://www.facebook.com/AITC4Meghalaya" target="_blank"><i class="fab fa-facebook-f"></i></a>
							<a href="https://twitter.com/AITC4Meghalaya" target="_blank"><i class="fab fa-twitter" target="_blank"></i></a>
							<a href="https://www.instagram.com/aitc4meghalaya/" target="_blank"><i class="fab fa-instagram" target="_blank"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</footer>



	<!--   Core JS Files   -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/sweetalert.min.js"></script>

	<script>
		$(document).ready(function() {
			setCookie('is_visit', '1', '30');

		});

		function setCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
			var expires = "expires=" + d.toUTCString();
			document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}

		//loader
		function ajaxindicatorstart(text) {
			$("#resultLoading").remove();
			if (jQuery(".custom-contain").find("#resultLoading").attr("id") != "resultLoading") {
				jQuery(".custom-contain").append(
					'<div id="resultLoading" style="display:none"><div><img src="<?php echo base_url() ?>assets/images/ajax-modal-loading.gif"><div>' +
					text +
					'</div></div><div class="bg"></div></div>'
				);
			}

			jQuery("#resultLoading").css({
				width: "100%",
				height: "100%",
				position: "fixed",
				"z-index": "10000000",
				top: "0",
				left: "0",
				right: "0",
				bottom: "0",
				margin: "auto",
			});

			jQuery("#resultLoading .bg").css({
				background: "#000000",
				opacity: "0.7",
				width: "100%",
				height: "100%",
				position: "absolute",
				top: "0",
			});

			jQuery("#resultLoading>div:first").css({
				width: "250px",
				height: "75px",
				"text-align": "center",
				position: "fixed",
				top: "0",
				left: "0",
				right: "0",
				bottom: "0",
				margin: "auto",
				"font-size": "16px",
				"z-index": "10",
				color: "#ffffff",
			});

			jQuery("#resultLoading .bg").height("100%");
			jQuery("#resultLoading").fadeIn("slow");
			jQuery(".container-contact100").css("cursor", "wait");
		}

		function ajaxindicatorstop() {
			jQuery("#resultLoading .bg").height("100%");
			jQuery("#resultLoading").fadeOut("slow");
			jQuery(".container-contact100").css("cursor", "default");
		}
		//end loader
	</script>

</body>

</html>