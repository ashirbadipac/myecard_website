<?php
defined('BASEPATH') or exit('No direct script access allowed');
$current_lang = $_SESSION['set_language'];
$default_lang = '';
if ($current_lang != 'en') {
    $default_lang = 'd-lang';
}

$rid = '';
if (isset($_GET['rid'])) {
    if (!empty($_GET['rid'])) {
        $rid = $_GET['rid'];
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MYE Card</title>
    <meta name="description" content="" />
    <meta name="keywords" content="TMC, AITC" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#29a2db" />
    <meta name="msapplication-navbutton-color" content="#29a2db" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#29a2db" />
    <meta name="robots" content="index, follow" />
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.png">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/color.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/register.css">
    <meta name="facebook-domain-verification" content="ed2kh007ps09shcrrmi6ewl24kkvzm" />
    <!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-PWY2JTVZB5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-PWY2JTVZB5');
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1980244685612613');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1980244685612613&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->  
<!-- Event snippet for TMC My E Card Sign Up Manually conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-722692676/4LkACJiV3YYYEMTUzdgC'});
</script>
<!-- Event snippet for MYE Card Sign-up conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-11059296387/oSwMCLmEnocYEIPxvZkp'});
</script>
<style>

</style>
</head>

<body>
    <!--loader-->
    <div class="loader-wrap">
        <div class="loader-inner">
            <div class="loader-inner-cirle"></div>
        </div>
    </div>
    <!--loader end-->
    <div id="main">
        <div id="wrapper">
            <div class="content">
                <!-- <div class="home-icon">
                <a href="<?php echo $current_lang == 'en' ? base_url() : base_url() . $current_lang ?>">Home</a>
            </div> -->
                <div class="lang-wrap">
                    <div class="show-lang">
                        <span>
                            <i class="fa fa-globe"></i>
                            <strong>
                                <?php echo $current_lang == 'en' ? $this->lang->line('lang_english') : ($current_lang == 'kha' ? $this->lang->line('lang_khasi') : $this->lang->line('lang_garo')); ?>
                            </strong>
                        </span>
                    </div>
                    <ul class="lang-tooltip no-list-style">
                        <?php
                        $string = '';
                        $string .= $rid != '' ? '?rid=' . $rid : '';
                        ?>
                        <li><a style="text-decoration:none" href="<?php echo base_url().'signup/' . $string ?>" class="<?php echo $current_lang == 'en' ? 'current-lan' : '' ?>"><?php echo $this->lang->line('lang_english'); ?></a></li>
                        <li><a style="text-decoration:none" href="<?php echo base_url() .'signup/'. 'garo' . $string; ?>" class="<?php echo $current_lang == 'garo' ? 'current-lan' : '' ?>"><?php echo $this->lang->line('lang_garo'); ?></a></li>
                        <li><a style="text-decoration:none" href="<?php echo base_url() .'signup/'. 'kha' . $string; ?>" class="<?php echo $current_lang == 'kha' ? 'current-lan' : '' ?>"><?php echo $this->lang->line('lang_khasi'); ?></a></li>
                    </ul>
                </div>
                <?php $desktop_header_banner = $current_lang == 'en' ? 'header_banner.jpg' : ($current_lang == 'kha' ? 'header_banner_kha.jpg' : 'header_banner_garo.jpg'); ?>
                <?php $mobile_header_banner = $current_lang == 'en' ? 'mobile_banner.jpg' : ($current_lang == 'kha' ? 'mobile_banner_kha.jpg' : 'mobile_banner_garo.jpg'); ?>
                <img class="w-100 desktop" src="<?php echo base_url(); ?>assets/images/<?php echo $desktop_header_banner; ?>" alt="WE Card">
                <img class="w-100 mobile" src="<?php echo base_url(); ?>assets/images/<?php echo $mobile_header_banner; ?>" alt="WE Card">
                <div class="step1" style="display:none">
                    <section>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <div class="account">
                                        <form id="regForm" name="regForm" method="POST" autocomplete="off">
                                            <div class="registration">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <h2 class="text-uppercase"><?php echo $this->lang->line('reg_tmc_mem'); ?></h2>
                                                            <label class="label-control"><?php echo $this->lang->line('reg_phone'); ?> <span class="asterisk">*</span></label>
                                                            <input class="form-control integer" type="text" name="phone_no" id="phone_no" placeholder="<?php echo $this->lang->line('reg_phone_ph'); ?>" maxlength="10" minlength="10">
                                                            <input type="hidden" id="language" name="language" value="<?php echo $current_lang ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <button id="begin_btn" type="button" class="btn custom-btn" name="begin_btn"><?php echo $this->lang->line('reg_begin'); ?></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="step2" style="display:none">
                    <section>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <div class="account">
                                        <form id="regOTP" name="regOTP" method="POST" autocomplete="off">
                                            <div class="registration">
                                                <div class="row justify-content-center <?php echo $default_lang ?>">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <h2 class="text-uppercase"><?php echo $this->lang->line('OTP_verify'); ?></h2>
                                                            <p class="w-s-t mb-2"><?php echo $this->lang->line('digit_text'); ?></p>
                                                            <label class="label-control"><?php echo $this->lang->line('reg_otp_enter'); ?></label>
                                                            <input id="otp" name="otp" type="text" class="form-control mb-2" placeholder="<?php echo $this->lang->line('reg_otp_enter'); ?>" required>
                                                            <a href="javascript:void()" id="generateOTP" class="text-underline" style="display: none;"><?php echo $this->lang->line('reg_otp_generate'); ?></a>
                                                        </div>
                                                        <div class="form-group">
                                                            <button id="verify_btn" type="button" class="btn custom-btn mb-2" name="verify_btn"><?php echo $this->lang->line('otp_submit_btn'); ?></button><br />
                                                            <?php echo $this->lang->line('reg_can_gen_otp'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="step3" style="displays:none">
                    <section class="py-0">
                        <div class="container">
                            <div class="row justify-content-center mt-0">
                                <h2 class="survey_heading mx-5 mt-4 mb-1 text-center"><?php echo $this->lang->line('form_heading'); ?></h2>
                                <div class="px-1 col-12 col-sm-12 col-md-8 col-lg-8">
                                    <div class="card <?php echo $default_lang ?>">
                                        <form id="surveyForm" name="surveyForm" method="POST" autocomplete="off" enctype="multipart/form-data">
                                            <fieldset>
                                               
                                                <div class="form-card-bottom">
                                                    <p class="card_head" id="female_card_head"><?php echo $this->lang->line('provide_details'); ?></p>
                                                    <div class="form-group">
                                                        <label class="label-control"><?php echo $this->lang->line('name_label'); ?><span class="asterisk">*</span></label>
                                                        <input class="form-control" type="text" name="name" id="name" placeholder="<?php echo $this->lang->line('placeholder_name'); ?>">
                                                        <input class="form-control" type="hidden" name="referral_code" id="referral_code" value="<?php echo $rid; ?>">
                                                        <input type="hidden" id="language" name="language" value="<?php echo $current_lang ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control"><?php echo $this->lang->line('phone'); ?><span class="asterisk">*</span></label>
                                                        <input class="form-control integer" type="text" name="read_only_phone_no" id="read_only_phone_no" minlength="10" maxlength="10" placeholder="<?php echo $this->lang->line('placeholder_ph'); ?>">
                                                    </div>
                                                    <div class="form-group mb-4">
                                                        <label class="label-control"><?php echo $this->lang->line('whatsapp'); ?></label>
                                                        <input type="text" class="form-control integer" id="whatsapp_number" name="whatsapp_number" maxlength="10" minlength="10" placeholder="<?php echo $this->lang->line('whatsapp_ph'); ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class='label-control'><?php echo $this->lang->line('gender'); ?> <span class="asterisk">*</span></label>
                                                        <div class="d-flex justify-content-start">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="gender" id="genderMale" value="Male">
                                                                <label class="form-check-label mb-0" for="genderMale"><?php echo $this->lang->line('gender_male'); ?></label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="gender" id="genderFemale" value="Female">
                                                                <label class="form-check-label mb-0" for="genderFemale"><?php echo $this->lang->line('gender_female'); ?></label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="gender" id="genderOther" value="Other">
                                                                <label class="form-check-label mb-0" for="genderOther"><?php echo $this->lang->line('gender_other'); ?></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-4">
                                                        <label class="label-control"><?php echo $this->lang->line('age'); ?><span class="asterisk">*</span></label>
                                                        <input type="text" class="form-control integer" id="age" name="age" maxlength="2" minlength="2" placeholder="<?php echo $this->lang->line('age_ph'); ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control"><?php echo $this->lang->line('label_district'); ?> <span class="asterisk">*</span></label>
                                                        <select class="form-control selectpicker" data-live-search="true" data-size="4" name="district" id="district">
                                                            <option value="" hidden><?php echo $this->lang->line('select_district'); ?></option>
                                                            <?php
                                                            foreach ($data['district_lists'] as $ac) {
                                                                echo '<option value="' . $ac['district'] . '" data-params="' . $ac['district_code'] . '">' . $ac['district'] . '</option>';
                                                            }
                                                            ?>
                                                          
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control"><?php echo $this->lang->line('label_ac'); ?> <span class="asterisk">*</span></label>

                                                        <select class="form-control" id="ac" name="ac">
                                                            <option value=""><?php echo $this->lang->line('select_ac'); ?></option>
                                                        </select>
                                                    </div>
                                                    
                                                
                                                    
                                                    <div class="form-group">
                                                        <div class="form-check">
                                                            <input class="form-check-input" name="confirmation" type="checkbox" value="" id="confirmation" checked>
                                                            <label class="form-check-label" for="confirmation">
                                                                <?php echo $this->lang->line('terms_and_condition'); ?></a>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group text-center my-4">
                                                    <button type="button" class="btn btn-custom btn-finish font-weight-bold m-auto d-block"><?php echo $this->lang->line('submit_btn'); ?></button>
                                                    <div class="progress mt-3 mx-3" style="display: none;">
                                                        <div class="progress-bar"></div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
             <!-- Start: About Section -->
             <div class="about">
                  <section>
                     <div class="container">
                        <div class="row justify-content-center">
                           <div class="col-md-12 col-12">
                              <h2 class="survey_heading"> <?php echo $this->lang->line('about'); ?></h2>
                           </div>
                          
                           <div class="col-md-12 d-flex align-items-center">
                              <p class="text-center"><?php echo $this->lang->line('about_text'); ?></p>
                           </div>
                        </div>
                     </div>
                  </section>
               </div>
               <!-- End: About Section -->
        </div>
        
        <footer class="main-footer fl-wrap">
            <img class="w-100" src="<?php echo base_url(); ?>assets/images/footer_<?php echo $current_lang ?>.jpg" alt="WE Card">
            <div class="sub-footer fl-wrap">
                <div class="container">
                    <div class="copyright"><?php echo $this->lang->line('copyright'); ?></div>
                    <div class="subfooter-nav">
                        <ul class="no-list-style">
                            <li class="text-white social-footer">
                                <?php echo $this->lang->line('follow_text'); ?>&nbsp;
                                <a href="https://www.facebook.com/AITC4Meghalaya" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                <a href="https://twitter.com/AITC4Meghalaya" target="_blank"><i class="fab fa-twitter" target="_blank"></i></a>
                                <a href="https://www.instagram.com/aitc4meghalaya/" target="_blank"><i class="fab fa-instagram" target="_blank"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>