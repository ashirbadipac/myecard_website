<!DOCTYPE html>
<html lang="ta">

<head>
    <base href="<?php echo base_url(); ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Ellorum Nammudan</title>
    <style>
        .container {
            max-width: 680px;
            min-width: 320px;
            padding: 10px;
            background-color: #d9d9d9;
            margin: 0px auto;
            text-align: center;
        }

        .img-responsive {
            width: 100%
        }

        .custom-btn {
            outline: none;
            box-shadow: none;
            text-decoration: none;
        }

        .custom-btn img {
            width: 34%;
        }

        .footer {
            margin: 10px 0px 0px 0px;
        }

        @media (max-width: 600px) {
            .custom-btn img {
                width: 50%;
            }
        }
    </style>
</head>

<body>
    <div class="container">
        <img class="img-responsive" style="display: block;" src="<?php echo base_url(); ?>assets/images/stalin_ani_emailer.jpg" />
        <div class="row" style="background-image: url('<?php echo base_url(); ?>assets/images/stalin_ani_bottom.jpg');background-repeat: no-repeat;background-position: top;background-size:cover">
            <a href="https://stalinani.page.link?apn=com.stalinani&ibi=com.ipac.stalinani&link=https%3A%2F%2Fstalinani.page.link%2F%3Freferral_code%3DSTLN6336MEM" target="_blank" class="custom-btn">
                <img src="<?php echo base_url(); ?>assets/images/stalin_ani_download.png" />
            </a>
            <div class="footer">
                <img src="<?php echo base_url(); ?>assets/images/play-store.png" />
            </div>
        </div>
    </div>
</body>

</html>