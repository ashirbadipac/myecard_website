<!DOCTYPE html>
<html lang="ta">

<head>
    <base href="<?php echo base_url(); ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Goenchi Navi Sakal</title>
    <style>
        .container {
            max-width: 680px;
            min-width: 320px;
            margin: 0px auto;
            text-align: center;
        }

        .img-responsive {
            width: 100%
        }

        .fieldset {
            width: 80%;
            margin: 30px auto;
            background: #f0f0f0;
            border-radius: 30px;
            padding-top: 15px;
        }

        .icon-img {
            position: absolute;
            top: 30px;
            width: 60px;
        }

        .row {
            padding: 30px;
        }

        .desc {
            background-color: #f0f0f0;
            padding: 0px 10px 15px 10px;
            border-radius: 30px;
        }

        .custom-btn {
            background-color: #ab1919;
            text-decoration: none;
            color: #fff !important;
            border-radius: 10px;
            padding: 5px 10px;
            font-size: 1rem;
            box-shadow: #ccc 0.01em 0.01em 0, #ccc 0.02em 0.02em 0, #ccc 0.03em 0.03em 0, #ccc 0.04em 0.04em 0, #ccc 0.05em 0.05em 0, #ccc 0.06em 0.06em 0, #ccc 0.07em 0.07em 0, #ccc 0.08em 0.08em 0, #ccc 0.09em 0.09em 0, #ccc 0.1em 0.1em 0;
        }

        .desc p {
            margin-bottom: 10px;
        }

        @media (max-width: 600px) {
            .fieldset {
                width: 100%;
            }

            .row {
                padding: 30px 15px;
                margin: -10px 0px 0px 0px;
            }

            .desc p {
                font-size: 0.8rem;
            }

            .custom-btn {
                font-size: 15px;
            }

            .footer p {
                font-size: 0.8rem;
            }
        }
    </style>
</head>

<body>
    <div class="container">
        <img class="img-responsive" style="display: block;" src="http://dmk.in/joindmk/assets/images/email-bg-1.png" />
        <div class="row" style="background-image: url('http://dmk.in/joindmk/assets/images/email-bg-2.png');background-repeat: no-repeat;background-position: center;">
            <div class="fieldset">
                <img class="icon-img" src="http://dmk.in/joindmk/assets/images/add-member.png" alt="">
                <div class="desc">
                    <p>திமுகவில் இணைந்தமைக்கு நன்றி. உங்கள் குடும்ப உறுப்பினர்களையும் கழகத்தில் இணைய பரிந்துரை செய்யுங்கள்</p>
                    <a href="http://dmk.in/joindmk/addmember" target="_blank" class="custom-btn">இங்கு அழுத்தவும்</a>
                </div>
            </div>
            <div class="fieldset">
                <img class="icon-img" src="http://dmk.in/joindmk/assets/images/stalin-ani.png" alt="">
                <div class="desc">
                    <p>திரு. மு.க.ஸ்டாலின் அவருடன் நேரடி தொடர்பில் இருங்கள் பரிசுகளை வெல்லுங்கள். பதிவிறக்கவும் ஸ்டாலின் அணி</p>
                    <a href="https://stalinani.page.link?apn=com.stalinani&ibi=com.ipac.stalinani&link=https%3A%2F%2Fstalinani.page.link%2F%3Freferral_code%3DSTLN6336MEM" target="_blank" class="custom-btn">பதிவிறக்க</a>
                </div>
            </div>
            <div class="footer" style="margin-top:30px">
                <p style="color: #fff;">2020 திராவிட முன்னேற்றக் கழகம்</p>
            </div>
        </div>
    </div>
</body>

</html>