<?php
defined('BASEPATH') or exit('No direct script access allowed');

/** Thank you page */
$lang['welcome_to_tmc'] = 'Congratulations [NAME]! Nang.ni MYE Card namen chu.sokaha';
$lang['discalmer'] = 'Nang·ni form-o jeko gapate on·aha uagita membership card-ko bikotaha. Ia nang·ni gapatgimin-o maiba gualani dongode nang·ni aselan ong·aigen.';
$lang['thank_download_btn'] = 'Cardko Download ka·bo';
$lang['thank_download_welcome'] = 'Rimchaksoani chitti-ko download ka.bo';
$lang['thank_review_btn'] = 'Scheme detailrangko nitaibo';
$lang['thank_signup_btn'] = 'Direct Signup ka·bo';
$lang['thank_front_view'] = 'Mikkangchakao Niani';
$lang['thank_back_view'] = 'Janggilchipak Niani';
$lang['thank_join_watsapp'] = 'WATSAPP-o bak ra·bo';
$lang['thank_click_to_connect'] = 'To receive direct messages from Mamata Banerjee,<br/>join us on WhatsApp';
$lang['thank_join_btn'] = 'Bak Ra·bo';
$lang['reg_date'] = 'Segatani Tarik';
/** pop up  */
$lang['modal_text'] = "गोव्यातील युवकांच्या स्वप्नाला आकार देण्यासाठी गोवा टीएमसी ने 'युवा शक्ती कार्ड' लाँच केले आहे. गोव्यातील युवकांना   (18 - 45 वर्षे) 4% व्याजदराने रु. 20 लाखांचे क्रेडिट कार्ड ";
$lang['modal_button'] = 'आताच नोंदणी करा!';


/** Footer Section */
$lang['copyright'] = 'Copyright © 2023 Meghalaya Trinamool Congress';
$lang['follow_text'] = 'Follow us on:';
$lang['faq'] = 'FAQs';
$lang['tc'] = 'Terms of use';
$lang['privacy'] = 'Privacy Policy';
