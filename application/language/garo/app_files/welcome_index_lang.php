<?php
defined('BASEPATH') or exit('No direct script access allowed');
/** Header Section */
$lang['lang_english'] = 'English';
$lang['lang_garo'] = 'Garo';
$lang['lang_khasi'] = 'Khasi';
$lang['signup'] = 'Sign Up';
$lang['signup_url'] = base_url() . 'signup/garo';
$lang['support_all'] = 'सर्वांसाठी पाठिंबा दर्शविल्याबद्दल धन्यवाद';
$lang['support_glc'] = 'गृहलक्ष्मी कार्डसाठी तुमचा पाठिंबा दर्शवल्याबद्दल धन्यवाद';
$lang['support_ysc'] = 'युवाशक्ती कार्डला आपला पाठिंबा दर्शवल्याबद्दल धन्यवाद';
$lang['support_mgmh'] = 'माझे घर, मालकी हक्कासाठी आपला पाठिंबा दर्शवल्याबद्दल धन्यवाद';
/**Home header text */
// $lang['home_header_text'] = 'Goa TMC launched a new scheme called Griha Laxmi, to provide an assured monthly income<br/>support to every household. Under this scheme, a direct transfer of <span class="home-header-text">₹5,000 per month (₹60,000 yearly)</span><br/>will be made to a <span class="home-header-text">woman</span> of <span class="home-header-text">every household</span> as guaranteed income support';
// $lang['home_header_text_mobile'] = 'Goa TMC launched a new scheme called<br/>Griha Laxmi, to provide an assured monthly<br/>income support to every household.<br/>Under this scheme, a direct transfer of<br/><span class="home-header-text">₹5,000 per month (₹60,000 yearly)</span> will be made<br/>to a <span class="home-header-text">woman</span> of <span class="home-header-text">every household</span> as<br/>guaranteed income support';
$lang['logo-title'] = 'Meghalaya Trinamool Congress';
$lang['no_of_days'] = 'No. of Days';
$lang['total_support'] = 'Chadamberang MYE card-na se.gatokaha ';
/** About  Section */
$lang['about']= 'MYE CARD NI GIMIN';
$lang['about_text'] = "Chadamberangan mikkangchi silroro namroanina aro songsalo dingtanganiko ra.bana nanggiprangan ong.a. Ian sorkari-ni chadamberangna kangkare ka.e dakna nanganirang ong.a jedakode uamangni kakketgipa miksonganiko chu.sokatna man.gen.Meghalaya TMC namen ku.si ong.bee uiatenga jekon manderang an.tangtangni ku•monganiko on.achi a.dok-o sorkari rikna man.odechinga gitalgipa scheme-ko oprake on.gen. — TMC's Meghalaya Youth Empowerment (MYE) Scheme. Chadamberangni mikkangchi janngi tanganiko tengsudapataniko ba ka.donganiko on.a, ia scheme-ni ku.rachakanigita re.baengipa bilsi 5-ni gisepo lakh 3 kamrangko nakatate on.gen aro jaanti ₹1,000/-ko (bilsio ₹12,000/- ko) kamgri donggengipa chadamberangna (jemangan bilsi 21- 40 ona) on.gen.";

/** FAQ Section */
$lang['faqs'] = 'Sing·ronggipa Sing·anirang';
$lang['faq1-q'] = 'TMC-ni Meghalaya Youth Empowerment Scheme-ara maia?';
$lang['faq1-a'] = 'Ia Meghalaya Youth Empowerment Scheme-ara (MYF) re.baengipa bilsi 5-ni gisep-o lakh 3 kamrangko nakatate on.gen aro jaanti ₹1000-ko (bilsio ₹12,000-ko) kamgri donggengipa chadamberangna jerangan bilsia  21- 40 ong.achim uarangna tangkarang-ko on.a ku.rachakaha.  ';
$lang['faq2-q'] = 'Ia namgnilko man.ani niamrangara namen altuagipa ong.a';
$lang['faq2-a-0'] = 'Iani namgnirangara minggittim gnang:';
$lang['faq2-a-1'] = 'a.	Sakanti kam gri donggenggipa chadamberang jerangni bilsian 21-40 gapahachim uamang an.tangtangni bimungko se.gatna gita man.gen. ';
$lang['faq2-a-2'] = 'b.Ia scheme-ni namgnirangko man.a je mandean dorgasto galachim ua mande Meghalaya a.doko at.chikamgipa ong.na nangen. ';
$lang['faq2-a-3'] = 'c.Je mandean an.tangna janggi tangna cholko dakna dorgasto galachim ua mande mamung tangka paisani dakchakako ba a.dok sorkariniko ba skotong sorkarioniko tangka-ko ra.sroman.gipa ong.na man.jawa.';
$lang['faq3-q'] = 'Ia scheme-ni namgniara mairang?';
$lang['faq3-a-0'] = 'Iani Namgni-rangara: ';
$lang['faq3-a-1'] = 'a.Bilsi chu.sokgipa saksantian jao ₹1000-ko (bilsi gimikni ₹12000-ko) seokako man.a kragiparang man.gen';
$lang['faq3-a-2'] = 'b. Somoi-gita man.gen';
$lang['faq3-a-3'] = 'c. Darangkoba mikkang nitegija on.gen ';
$lang['faq3-a-4'] = 'd. Man.a nanganiko chotchanggija man.gen';
$lang['faq4-q'] = 'Maikai ia MYE scheme-ara gipin scheme-rang baksa dingtanggrikani donggenchim? ';
$lang['faq4-a'] = 'Meghalaya a.dok-o donggengipa scheme-rangara kamgri doonggenggipa chadamberangna tangka paisa-ko on.e dakchake on.nani palo donggipa manderangnasa  dakchakskae on.enga. Indioba ia  MYE  scheme-o  kamgri donggenggipa chadamderangna lakh 3 kamrangko bilsi 5-ni gisep-o nakatate on.gen  jeon jaanti ₹1000-ko (bilsi gimikni ₹12,000-ko) jerangan bilsi  21-40 ona gapahachim uamangna on.a ku.rachakaha.';
$lang['faq5-q'] = 'Ia tangka-ko anga jaanti man.agita  maikobarang dakna nanggenma? ';
$lang['faq5-a'] = 'Ong.ja, ia chu.gimik man.a nangani tangkarang-ko kamgri donggenggipa chadamberangna on.a ku.rachakaha jerangani bilsi 21-40 ona, joljol watatgen.   ';
$lang['faq6-q'] = 'Se.gatgimin chadamberang man.a nangniragko man.a gita bank account dongna nangenma?';
$lang['faq6-a'] = 'Nango bank account dong.jagenchim ong.oba  na.a man.a nangnirangko man.aigen. Jensalo Meghalaya TMC 2023 bilsio sorkari rikgen, seokako man.gimin mande man.a nangnirangko dao gapatgimino pangchake man.a dakchake on.gen.';
$lang['faq7-q'] = 'MYE card-ko don.guala ba gimaata ong.ode maiko dakgen?';
$lang['faq7-a'] = 'MYE card-ko name simsake dona gita agane on.gen. Indioba, chingo Unique Identification Number (UID) sakkantini se.gatmitingo SMS ba online confirmation gita ka.gipa donga, jean mikkangchi card-ko jakkalpilna gita man.gen.';

$lang['faq8-q'] = 'Basak-o ia namgnirangko man.anira a.bachenggen? ';
$lang['faq8-a'] = 'Manderangni patianiko man.gen jensalo TMC Meghalaya a.dok-o sorkari rikgen unon ia scheme-ni namgnirangko kamgri donggenggipa chadamberang jemangan seokako man.a kraa uamang man.a nanganirangko man.gen. ';
$lang['faq9-q'] = 'Se.gatani somoio ang.a antangni gimin sakki ba uiataniko on.a nangenma?';
$lang['faq9-a'] = 'Nangjawa, na.a nangni segatani somoio mamung an.tangni gimin sakki ba uitaniko on.agita nangjawa';

$lang['faq10-q'] = 'Anga MYE Scheme-ni namgnirangkora na.songni Party-o namnike naposan man.aigenma?';
$lang['faq10-a'] = 'Ong.ja, jerangan  kamgri ong.e Meghalaya a.doko songdongenggipa chadamberang bilsi 21-40-ona, sorkario bebe ra.anio jak dongpani gri ba party-o napani gri card-na se.gatnagita man.gen.';

$lang['faq11-q'] = 'Maike Tangka Paisa on.ani kamrangara ong.gen? Iakora bilsio changsasan on.genma?';
$lang['faq11-a'] = 'Ong.ja, man.a nanggnirangko jaanti man.gen.Jerangan kamgri ong.e donggenggipa chadamberang se.gatmanahachim uamang bilsi 21-40-ona jaanti joljol ₹1,000-ko (bilsio gimik-ni ₹12,000-ko) man.gen.';

$lang['faq12-q'] = 'Maike paisa on.nani kam ka.anirangara ong.gen? Iakora bilsio changsasan man.aigenma? ';
$lang['faq12-a'] = 'Ong.ja ia man.na nangni paisarangko ja.rikit on.gen. Gimik nokdangrangan jemang register ka.ahachim uamang ja.o gong 1000/- tangkarangko man.gen (bilsio chanatode 12,000 tangkarang gapgen)';

$lang['faq13-q'] = 'Chu.gimik tangka paisa man.anirangko anga jakon man.genma?';
$lang['faq13-a'] = 'Ong.ja, mamung dontonganigri account-ona joljol watatgen.';

$lang['faq14-q'] = 'Mamung bank account-ni number-gri ba sakki gri, party maidake man.a nanggnirangko on.gen?';
$lang['faq14-a'] = 'Party ia scheme-ko kam ka.kujawa. Manderangni pattianichi jensalo TMC sorkari rikgen unosa ia scheme-ko kam ka.gen. Seokako man.gimin sorkarini mande se.gatgimin manderangni on.gimin sakkio pangchake sokatgen.';

$lang['faq15-q'] = 'Tolgipa Pin-ko man.soe aro bank account-ni chu.gimk nanganirangko  maibakai sakgippinna parak ong.ode mai ong.gen? ';
$lang['faq15-a'] = 'Mamung bank account-ni chu.gimik nanganirangko am.jawa unigimin gipatani mamung chol dongjawa. MYE Card-na se.gatgipa mandesan PIN-ko mansogen. Meghalaya TMC mamung saloba bank account-ni gimin call kae singatjawa, maibakai saoba call ka.ata ong.ode uko agane on.bo.';






/**registration form */
$lang['provide_details'] = 'Nang.ni gmin tale gapatbo';
$lang['female_details'] = "Nokdangni me.chik dal.batgipa-ko talatbo  ";
$lang['reg_tmc_mem'] = 'MYE  Card register form';
$lang['video_guide_header'] = 'Registration Guide';
$lang['reg_phone'] = 'Nang·ni Mobile Numberko gapatbo';
$lang['reg_phone_ph'] = '10 digit numberko gapatbo';
$lang['reg_begin'] = 'Segatniko a·bachengbo';
$lang['how_to_how'] = 'How to Register?';
$lang['enter_mob_for_reg'] = 'Step 1: Enter Mobile Number to begin registration';
$lang['enter_pin'] = 'Step 2: Enter OTP to confirm your mobile number ';
$lang['fill_details'] = 'Step 3: Fill the required details';
$lang['get_your_card'] = 'Step 4: Get your <span class="yellow">Griha Laxmi Card</span>';
$lang['know_more'] = '<i class="text-white">To Know more click <a id="faq" class="yellow" target="_blank" href="faq/faq.html"><u>FAQs</u></a>  or Call us on </i> <b class="ph_yellow">97620 97620</b> ';

/**Enter OTP form */
$lang['OTP_verify'] = 'OTP ko ong·ama ong·ja niani';
$lang['digit_text'] = 'Nang·ni mobile numberona 5 digit OTPko SMS<span id="otp_phone_no"></span>  gita watatahav';
$lang['reg_otp_enter'] = 'OTPko gapatbo';
$lang['reg_otp_generate'] = 'OTPko bikotbo';
$lang['reg_can_gen_otp'] = '<div class="timer_div" style="display: none;">90 secondo OTPko<span id="timer"></span>  bikotaina man·gen</div>';
$lang['otp_submit_btn'] = 'Submit ka·bo';

/**personal details */
/**personal details */
$lang['form_heading'] = 'Nang.ni gimin gapatbo. ';
$lang['name_label'] = 'Bimung ';
$lang['placeholder_name'] = 'Nang·ni Chu·gimik Bimung';
$lang['phone'] = 'Mobile Number';
$lang['placeholder_ph'] = 'Your Mobile Number';
$lang['gender'] = 'Me·a/Me·chik';
$lang['gender_male'] = 'Me.asa';
$lang['gender_female'] = 'Me.chik ';
$lang['gender_other'] = 'Gipinrang';
$lang['female_head'] = 'Na.ara mechik dalbatgipa ma noko ni? ';
$lang['yes'] = 'Ong·a';
$lang['no'] = 'Ong·ja';
$lang['female_head_name'] = 'Nokdangni me.chik dal.batgipa-ni bimung  ';
$lang['female_head_name_ph'] = 'Nokdangni me.chik dal.batgipa-ni bimung  gapatbo';
$lang['father_name'] = 'Father’s/Husband’s Name';
$lang['father_placeholder'] = 'Your Father’s/Husband’s Name';
$lang['age'] = 'Age of Female Head';
$lang['select_age'] = 'Select Age';
$lang['Female_head_phone'] = "Phone Number";
$lang['Female_head_phone_ph'] = "Enter phone number";
$lang['whatsapp'] = 'WhatsApp Number';
$lang['whatsapp_ph'] = 'Enter Your WhatsApp Number';
$lang['bank_account'] = 'Do you have a bank account?';
$lang['adress'] = 'Nangni song nok ba dongengipa biap ko gapat bo';
$lang['adress_ph'] = 'Nangni song nok ba dongengipa biap ko gapat bo ';
$lang['pincode'] = 'Pin Code';
$lang['pincode_ph'] = 'Enter Pincode';
$lang['ward'] = 'Village Name/Ward Number';
$lang['ward_ph'] = 'Enter Village Name/Ward Number';
$lang['label_district'] = 'District';
$lang['select_district'] = 'District-ko seokbo';
$lang['label_ac'] = 'Assembly Constituencyko';
$lang['select_ac'] = 'Assembly Constituencyko seokbo';

$lang['label_block'] = 'Block/Taluka';
$lang['select_block'] = 'Select Block/Taluka';
$lang['glc_card'] = 'Na.a MYE Card-ko man.ahama?';
$lang['glc_cardno'] = 'dingtanggipa identification number-ko (UID) japa ka.giminko gapatbo';
$lang['glc_cardno_ph'] = 'Unique Identification Number (UID)-ko gapatbo';
$lang['terms_and_condition'] = 'Ang.a gimik <a href="' . base_url() . 'terms-and-conditions.html" target="_blank"><u>dakna nang.anikon ra.chaka</u></a>';
$lang['submit_btn'] = 'Submit ka·bo';
$lang['dob'] = 'Atchiani sal/tarik';
$lang['dob_year'] = 'Year';
$lang['dob_month'] = 'Ja';
$lang['dob_month_1'] = 'Roro';
$lang['dob_month_2'] = 'Dongro';
$lang['dob_month_3'] = 'Galmak';
$lang['dob_month_4'] = 'Mige';
$lang['dob_month_5'] = 'Kilge';
$lang['dob_month_6'] = 'Bandoni';
$lang['dob_month_7'] = 'Wasosi';
$lang['dob_month_8'] = 'Sampang';
$lang['dob_month_9'] = 'Micha';
$lang['dob_month_10'] = 'Wanma';
$lang['dob_month_11'] = 'Jabilsi';
$lang['dob_month_12'] = 'Sil Ginchi';
$lang['dob_day'] = 'Sal';
$lang['mandatory_fields'] = '* - Nangchongmotgipa Biaprang';
$lang['age'] = 'Age';
$lang['age_ph'] = 'Nang.ni bilsi ko gapatbo ';
/** Footer Section */
$lang['copyright'] = 'Copyright © 2023 Meghalaya Trinamool Congress';
$lang['follow_text'] = 'Iano chingko ja·rikbo:';
$lang['faq'] = 'FAQs';
$lang['faq_link'] = base_url() . 'faq.html';
$lang['tc'] = 'Terms of use';
$lang['privacy'] = 'Privacy Policy';
