<?php
defined('BASEPATH') or exit('No direct script access allowed');
/** Header Section */
$lang['lang_english'] = 'English';
$lang['lang_garo'] = 'Garo';
$lang['lang_khasi'] = 'Khasi ';
$lang['signup'] = 'Sign Up';
$lang['signup_url'] = base_url() . 'signup/';
$lang['support_all'] = 'Thank you for showing support for all';
$lang['support_glc'] = 'Thank you for showing your support for Grihalaxmi card';
$lang['support_ysc'] = 'Thank you for showing your support for Yuvashakti card';
$lang['support_mgmh'] = 'Thank you for showing your support for Mhaje Ghar, Maalki Hakk';
/**Home header text */
// $lang['home_header_text'] = 'Goa TMC launched a new scheme called Griha Laxmi, to provide an assured monthly income<br/>support to every household. Under this scheme, a direct transfer of <span class="home-header-text">₹5,000 per month (₹60,000 yearly)</span><br/>will be made to a <span class="home-header-text">woman</span> of <span class="home-header-text">every household</span> as guaranteed income support';
// $lang['home_header_text_mobile'] = 'Goa TMC launched a new scheme called<br/>Griha Laxmi, to provide an assured monthly<br/>income support to every household.<br/>Under this scheme, a direct transfer of<br/><span class="home-header-text">₹5,000 per month (₹60,000 yearly)</span> will be made<br/>to a <span class="home-header-text">woman</span> of <span class="home-header-text">every household</span> as<br/>guaranteed income support';
$lang['logo-title'] = 'Meghalaya Trinamool Congress';
$lang['no_of_days'] = 'No. of Days';
$lang['total_support'] = 'Youth have registered for MYE card';
/** About  Section */
$lang['about']= 'About MYE Card';
$lang['about_text'] = 'Youth is the catalyst for progress and the future of any society. It’s a responsible government’s duty to provide necessary avenues and to support them to reach their true potential.Meghalaya TMC is excited to announce that if people give us their blessings to form the government in Meghalaya, we will launch a new scheme — TMC’s Meghalaya Youth Empowerment (MYE) Scheme. To ensure a brighter future for the youth, the scheme guarantees to create 3 lakh jobs over the next 5 years and provide an allowance of ₹1,000 monthly (₹12,000 yearly) to all unemployed youth between ages of 21-40 years.
';

/** FAQ Section */
$lang['faqs'] = 'Frequently Asked Questions';
$lang['faq1-q'] = 'What is TMC’s Meghalaya Youth Empowerment (MYE) Scheme?';
$lang['faq1-a'] = 'TMC’s Meghalaya Youth Empowerment (MYE) Scheme guarantees to create 3 lakh jobs over the next 5 years and provide an allowance of ₹1,000 monthly (₹12,000 yearly) to every unemployed youth between the ages of 21-40 years.';
$lang['faq2-q'] = 'Is there any eligibility criteria for availing the benefit?';
$lang['faq2-a-0'] = 'The eligibility criteria for the scheme is very simple:';
$lang['faq2-a-1'] = 'a.	Every unemployed youth within the age group of 21-40 years can register';
$lang['faq2-a-2'] = 'b.The applicant should be a native of Meghalaya to receive the benefits of the scheme';
$lang['faq2-a-3'] = 'c.The applicant should not avail any financial assistance or loan under State or Central Governments relating to self-employment scheme';
$lang['faq3-q'] = 'What are the benefits of this scheme?';
$lang['faq3-a-0'] = 'The benefits are:';
$lang['faq3-a-1'] = 'a. ₹1,000 monthly (₹12,000 yearly) to every eligible person';
$lang['faq3-a-2'] = 'b. Periodic';
$lang['faq3-a-3'] = 'c. Non-discriminatory';
$lang['faq3-a-4'] = 'd. Unconditional direct transfer';
$lang['faq4-q'] = 'How will you ascertain that the targeted beneficiary is unemployed?';
$lang['faq4-a'] = 'A well-defined procedure will be followed to verify whether the registered individual meets the eligibility criteria or not.
';
$lang['faq5-q'] = 'How is the MYE Scheme different from other existing schemes?';
$lang['faq5-a'] = 'The existing schemes in Meghalaya are aimed towards capacity building rather than direct financial assistance to unemployed youth. On the other hand, the MYE Scheme guarantees to create 3 lakh jobs over the next 5 years and provide an allowance of ₹1,000 monthly (₹12,000 yearly) to every unemployed youth between the ages of 21-40 years.';
$lang['faq6-q'] = 'Is there any task that I should complete every month to receive the money?';
$lang['faq6-a'] = 'No, this amount will be directly transferred as an assured allowance to every unemployed youth of the state between the ages of 21-40 years';
$lang['faq7-q'] = 'Does the registered youth need to have a bank account to avail this benefit?';
$lang['faq7-a'] = 'If you do not have a bank account, you can still receive the benefit. Once Meghalaya TMC forms the government in 2023, an official will guide you through the process to receive funds, based on the details we collect now.';

$lang['faq8-q'] = 'What if the MYE Card is misplaced or lost?';
$lang['faq8-a'] = 'It is advised to keep the card safely. However, we have the Unique Identification Number (UID) of everyone registering through SMS or online confirmation, which can be used to reissue the card in the future.';
$lang['faq9-q'] = 'When can the beneficiary start availing this benefit?';
$lang['faq9-a'] = 'With people’s blessings, as soon as TMC forms the government in Meghalaya, the scheme will be implemented, and every unemployed youth meeting the scheme’s eligibility criteria can start availing this benefit.';

$lang['faq10-q'] = 'Do I need to provide any identification proof during the time of registration?';
$lang['faq10-a'] = 'No, you are not required to give any identification proof at the time of registration.';

$lang['faq11-q'] = 'Do I only get the benefit of the MYE Scheme if I agree to join your Party?';
$lang['faq11-a'] = 'No, any unemployed youth who is a resident of Meghalaya between the ages of 21-40 years can register for the card, irrespective of their political beliefs, or decision to join the Party.';
$lang['faq12-q'] = 'What will be the procedure of payment? Is it going to be once a year?';
$lang['faq12-a'] = 'No, the stipulated amount will be given on a monthly basis. So every registered unemployed youth between the ages of 21-40 years will receive a direct transfer of ₹1,000 monthly (₹12,000 yearly).';
$lang['faq13-q'] = 'Will I receive the amount in cash?';
$lang['faq13-a'] = 'No, it will be an unconditional direct bank transfer.';
$lang['faq14-q'] = 'Without details like bank account numbers, identification cards, how will the party  allocate the beneficiary fund?';
$lang['faq14-a'] = 'The party will not be implementing the scheme. With people’s blessings, when the Meghalaya TMC forms the government, the scheme will be implemented. Authorized Government officials will reach out to the registrants based on the details provided now.
';
$lang['faq15-q'] = 'What if the PIN received results in fraud, and our bank account details are leaked?';
$lang['faq15-a'] = 'No bank account details will be collected and therefore no chances of leaks. The PIN received is only for the authentication of the MYE Card. Meghalaya TMC will not call anyone for bank details, kindly report if someone asks for the same.';
$lang['faq16-q'] = 'Will benefits from the current schemes stop if we register for the MYE Scheme?';
$lang['faq16-a'] = 'All schemes that secure the rights of the people of Meghalaya will be continued.';
$lang['faq17-q'] = 'Is there a website? Can we register online?';
$lang['faq17-a'] = 'Yes, you may register for the scheme online. The website URL is <a href="https://www.tmcwecard.com/">www.tmcwecard.com.</a>';
$lang['faq18-q'] = 'What if the PIN received results in fraud, and our bank account details are leaked?';
$lang['faq18-a'] = 'No bank account details will be collected and therefore no chances of leaks. The PIN received is only for the authentication of the WE Card. Meghalaya TMC will not call anyone for bank details, kindly report if someone asks for the same.';
$lang['faq19-q'] = 'Will our current schemes and pensions be stopped, if we register for the MFI WE Scheme?';
$lang['faq19-a'] = 'All schemes that secure the rights of the people of Meghalaya will be continued.';

/**registration form */
$lang['provide_details'] = 'Provide your details';
$lang['female_details'] = "Details of Adult Female in the Family";
$lang['reg_tmc_mem'] = 'Register for WE Card';
$lang['video_guide_header'] = 'Registration Guide';
$lang['reg_phone'] = 'Enter Your Mobile Number';
$lang['reg_phone_ph'] = 'Enter 10 digit mobile number';
$lang['reg_begin'] = 'Begin Registration';
$lang['how_to_how'] = 'How to Register?';
$lang['enter_mob_for_reg'] = 'Step 1: Enter Mobile Number to begin registration';
$lang['enter_pin'] = 'Step 2: Enter OTP to confirm your mobile number ';
$lang['fill_details'] = 'Step 3: Fill the required details';
$lang['get_your_card'] = 'Step 4: Get your <span class="yellow">Griha Laxmi Card</span>';
$lang['know_more'] = '<i class="text-white">To Know more click <a id="faq" class="yellow" target="_blank" href="faq/faq.html"><u>FAQs</u></a>  or Call us on </i> <b class="ph_yellow">97620 97620</b> ';

/**Enter OTP form */
$lang['OTP_verify'] = 'OTP VERIFICATION';
$lang['digit_text'] = 'We have send 5-digit OTP to your mobile number <span id="otp_phone_no"></span> via SMS and IVR';
$lang['reg_otp_enter'] = 'Enter OTP';
$lang['reg_otp_generate'] = 'Generate OTP';
$lang['reg_can_gen_otp'] = '<div class="timer_div" style="display: none;">You can regenerate the OTP in <span id="timer"></span> seconds</div>';
$lang['otp_submit_btn'] = 'Submit';

/**personal details */
$lang['form_heading'] = 'MYE Card Registration Form';
$lang['name_label'] = 'Full Name';
$lang['placeholder_name'] = 'Your Full Name';
$lang['phone'] = 'Mobile Number';
$lang['placeholder_ph'] = 'Your Mobile Number';
$lang['gender'] = 'Gender';
$lang['gender_male'] = 'Male';
$lang['gender_female'] = 'Female';
$lang['gender_other'] = 'Other';
$lang['female_head'] = 'Are you adult female in the family?';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['female_head_name'] = 'Name of Adult Female in the Family';
$lang['female_head_name_ph'] = 'Enter Name of Adult Female in the Family';
$lang['father_name'] = 'Father’s/Husband’s Name';
$lang['father_placeholder'] = 'Your Father’s/Husband’s Name';
$lang['age'] = 'Age of Female Head';
$lang['select_age'] = 'Select Age';
$lang['Female_head_phone'] = "Phone Number";
$lang['Female_head_phone_ph'] = "Enter Phone Number";
$lang['whatsapp'] = 'WhatsApp Number';
$lang['whatsapp_ph'] = 'Enter Your WhatsApp Number';
$lang['bank_account'] = 'Do you have a bank account?';
$lang['adress'] = 'Enter Your Full Address ';
$lang['adress_ph'] = 'Enter Your Full Address ';
$lang['pincode'] = 'Pin Code';
$lang['pincode_ph'] = 'Enter Pincode';
$lang['ward'] = 'Village Name/Ward Number';
$lang['ward_ph'] = 'Enter Village Name/Ward Number';
$lang['label_district'] = 'District';
$lang['select_district'] = 'Select District';
$lang['label_ac'] = 'Assembly Constituency';
$lang['select_ac'] = 'Select Assembly Constituency';

$lang['label_block'] = 'Block/Taluka';
$lang['select_block'] = 'Select Block/Taluka';
$lang['glc_card'] = 'Have you received a MYE Card?';
$lang['glc_cardno'] = 'Enter the Unique Identification Number (UID) printed on your MYE card';
$lang['glc_cardno_ph'] = 'Enter the Unique Identification Number (UID)';
$lang['terms_and_condition'] = 'I agree to the <a href="' . base_url() . 'terms-and-conditions.html" target="_blank"><u>Terms & Conditions</u></a>';
$lang['submit_btn'] = 'Submit';
$lang['dob'] = 'Date of birth';
$lang['dob_year'] = 'Year';
$lang['dob_month'] = 'Month';
$lang['dob_month_1'] = 'January';
$lang['dob_month_2'] = 'February';
$lang['dob_month_3'] = 'March';
$lang['dob_month_4'] = 'April';
$lang['dob_month_5'] = 'May';
$lang['dob_month_6'] = 'June';
$lang['dob_month_7'] = 'July';
$lang['dob_month_8'] = 'August';
$lang['dob_month_9'] = 'September';
$lang['dob_month_10'] = 'October';
$lang['dob_month_11'] = 'November';
$lang['dob_month_12'] = 'December';
$lang['dob_day'] = 'Day';
$lang['mandatory_fields'] = '* - Mandatory Fields';
$lang['age'] = 'Age';
$lang['age_ph'] = 'Enter Your Age';
/** Footer Section */
$lang['copyright'] = 'Copyright © 2023 Meghalaya Trinamool Congress';
$lang['follow_text'] = 'Follow us on:';
// $lang['faq'] = 'FAQs';
$lang['faq_link'] = base_url() . 'faq.html';
$lang['tc'] = 'Terms of use';
$lang['privacy'] = 'Privacy Policy';
