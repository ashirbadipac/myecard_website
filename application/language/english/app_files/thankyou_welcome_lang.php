<?php
defined('BASEPATH') or exit('No direct script access allowed');

/** Thank you page */
$lang['welcome_to_tmc'] = 'Congratulations [NAME]! Your MYE Card is successfully activated.';
$lang['discalmer'] = 'This membership card was generated based on the information shared by yourself in the form. In case if any of this information is found to be false, you will be liable for any action that the party might undertake';
$lang['thank_download_btn'] = 'Download the Card';
$lang['thank_download_welcome'] = 'Download Welcome Letter';
$lang['thank_review_btn'] = 'Review Scheme Details';
$lang['thank_signup_btn'] = 'Signup Directly';
$lang['thank_front_view'] = 'Front View';
$lang['thank_back_view'] = 'Back View';
$lang['thank_join_watsapp'] = 'JOIN WHATSAPP';
$lang['thank_click_to_connect'] = 'To receive direct messages from Mamata Banerjee,<br/>join us on WhatsApp';
$lang['thank_join_btn'] = 'Join';
$lang['reg_date'] = 'Registration Date';


/** pop up  */
$lang['modal_text'] = "Goa TMC has launched 'Yuva Shakti Card' to give shape to the dream of Goan youth. A credit card of INR 20 lakh at a 4% interest rate to Goan youth (18 - 45 yrs)";
$lang['modal_button'] = 'Register now!';

// $lang['find_your_card'] = 'Please find your Online TMC Membership card below';
// $lang['thank_name'] = 'Name';
// $lang['thank_father_name'] = 'Father/Husband Name';
// $lang['thank_age'] = 'Age';
// $lang['thank_joining_date'] = 'Date of Joining';
// $lang['thank_ac'] = 'Assembly Constituency';
// $lang['thank_district'] = 'District';
// $lang['address'] = 'Address:';
// $lang['thank_mem_id'] = 'Membership ID';
// $lang['thank_front_view'] = 'Front View';
// $lang['thank_back_view'] = 'Back View';
//$lang['thank_download_id'] = 'To download a PDF version of your ID card, click on the link below';
//$lang['thank_download_btn'] = 'Download the Card';
//$lang['thank_join_btn'] = 'Join';
//$lang['thank_click_to_connect'] = 'To receive direct messages from Mamata Banerjee,<br/>join us on WhatsApp';
// $lang['mamata_download'] = 'To stay connected with Mamata Banerjee’s Office, download <b>Goa Deserves Better</b>';
// $lang['mamata_download_btn'] = 'Download';
// $lang['no_of_referral'] = 'No. of people registered using your referral code';
// $lang['thank_step1'] = 'Step1';
// $lang['thank_step2'] = 'Step2';
// $lang['thank_step3'] = 'Step3';
// $lang['edit'] = "Edit/Review Details";
// $lang['next_steps'] = 'Next Steps';
// $lang['back_to_home'] = 'Back to Home';
// $lang['your_referral'] = 'Your Referrals';
// $lang['discalmer'] = 'This membership card was generated based on the information shared by yourself in the form. In case if any of this information is found to be false, you will be liable for any action that the party might undertake';
// $lang['click_here'] = 'Click Here';
// $lang['family_member'] = 'To add a family member';
// $lang['family_member_count'] = '(* You can add upto 3 members)';
// $lang['family_member_error'] = '* Maximum number of members added';

/** Footer Section */
$lang['copyright'] = 'Copyright © 2023 Meghalaya Trinamool Congress';
$lang['follow_text'] = 'Follow us on:';
$lang['faq'] = 'FAQs';
$lang['tc'] = 'Terms of use';
$lang['privacy'] = 'Privacy Policy';
