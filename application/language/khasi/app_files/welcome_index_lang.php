<?php
defined('BASEPATH') or exit('No direct script access allowed');
/** Header Section */
$lang['lang_english'] = 'English';
$lang['lang_garo'] = 'Garo';
$lang['lang_khasi'] = 'Khasi';
$lang['signup'] = 'Pyndap';
$lang['signup_url'] = base_url() . 'signup/kha';
$lang['support_all'] = 'सगल्यांक फाटबळ दिलें म्हण दिनवास';
$lang['support_glc'] = 'गृहलक्ष्मी कार्डाखातीर तुमीं फाटबळ दिलें म्हण दिनवास';
$lang['support_ysc'] = 'युवा शक्ति कार्डाखातीर तुमीं फाटबळ दिलें म्हण दिनवास';
$lang['support_mgmh'] = 'म्हजें घर, मालकी हक्क हाचेखातीर तुमीं फाटबळ दिलें म्हण दिनवास';
/**Home header text */
// $lang['home_header_text'] = 'Goa TMC launched a new scheme called Griha Laxmi, to provide an assured monthly income<br/>support to every household. Under this scheme, a direct transfer of <span class="home-header-text">₹5,000 per month (₹60,000 yearly)</span><br/>will be made to a <span class="home-header-text">woman</span> of <span class="home-header-text">every household</span> as guaranteed income support';
// $lang['home_header_text_mobile'] = 'Goa TMC launched a new scheme called<br/>Griha Laxmi, to provide an assured monthly<br/>income support to every household.<br/>Under this scheme, a direct transfer of<br/><span class="home-header-text">₹5,000 per month (₹60,000 yearly)</span> will be made<br/>to a <span class="home-header-text">woman</span> of <span class="home-header-text">every household</span> as<br/>guaranteed income support';
$lang['logo-title'] = 'Meghalaya Trinamool Congress';
$lang['no_of_days'] = 'No. of Days';
$lang['total_support'] = 'Samla ba lah dep register bynta ka MYE Card';

/** About  Section */
$lang['about']= 'Shaphang ka MYE Card';
$lang['about_text'] = 'Ki samla ki dei kiba kongsan eh ha ka bynta ban kyntiew ia ka lawei jong ka imlang sahlang. Ka dei ka kamram jong ka Sorkar kaba don jingkitkhlieh ban plie ki lad bad ban kyrshan ia ki samla ba kin poi sha ki thong jong ki.Ka Meghalaya TMC ka sngewkmen ban pyntip ba lada ki briew ki ai ia ka jingkyrkhu kyrdoh ba ngin thaw sorkar ha Meghalaya, ha u snem 2023, ngin sa pynmih ka scheme bathymmai -TMC Meghalaya Youth Empowerment (MYE) Scheme. Ban pynthikna ia ka lawei baphyrnai jong ki samla, kane ka scheme ka kular ban pynmih kumba 3 lak tylli ki Kam ha kine ki 5 snem ki ban sa wan bad ban ai bai bnai T. 1,000 shibnai bad (T.12,000 shi snem) ia baroh ki samla ki ba bym pat ioh kam (21-40 snem)  ';

/** FAQ Section */
/** FAQ Section */
$lang['faqs'] = 'Ki jingkylli ba kylli barabor';
$lang['faq1-q'] = 'Kaei kata ka Meghalaya Youth Empowerment (MYE) Scheme ba lah pynmih da ka TMC?
';
$lang['faq1-a'] = 'Ka Meghalaya Youth Empowerment (MYE) Scheme ba lah pynmih da ka TMC ka pynthikna ban pynmih haduh 3 lak ki jait Kam ha kine ki 5 snem ban wan bad ban pynioh T.1000 shi bnai (T. 12,000 shi snem) ia baroh ki samla ki bym pat ioh kam kiba hap hapoh ka rta 21-40 snem.
';
$lang['faq2-q'] = 'Kiei ki kyndon ban ioh ia kine ki jingmyntoi?';
$lang['faq2-a-0'] = 'Ki kyndon ban ioh jingiarap na kane ka scheme ki long:';
$lang['faq2-a-1'] = 'a.	Baroh ki samla kiba hapoh ka rta kaba 21-40 snem ki lah ban register.';
$lang['faq2-a-2'] = 'b.	Ki ba register ki dei ban long ki trai shnong jong ka Meghalaya ban ioh pdiang ia ki jingiarap na kane ka scheme
';
$lang['faq2-a-3'] = 'c.Kiba register kim lang ban dei kiba lah ioh jingiarap lane shim ram na ki Self-Employment Scheme jong ka sorkar Jylla ne sorkar Pdeng 
';
$lang['faq3-q'] = 'Kiei ki jingmyntoi ban ioh na kane ka scheme?';
$lang['faq3-a-0'] = 'Ki jingmyntoi ki long:';
$lang['faq3-a-1'] = 'a. T.1000 sh ibnai (T.12,000 shi snem) ia baroh ki samla ki bym pat ioh kam kiba hap hapoh ka rta 21-40 snem.
';
$lang['faq3-a-2'] = 'b. Ka long kaba tista';
$lang['faq3-a-3'] = 'c. Ka bym peit shiliang iano iano';
$lang['faq3-a-4'] = 'd. Ka jingphah pisa khlem kino kino ki kyndon';
$lang['faq4-q'] = 'Kumno ka MYE scheme ka iapher na kiwei pat ki scheme?';
$lang['faq4-a'] = 'Ki scheme kiba lah don lypa ki dei tang ban iarap lane pynbha  ia ka kam kaba lah don lypa hynrei ym ban pynpoi beit ia ka jingiarap lyngba ka pisa sha ki samla bym pat ioh kam. Hynrei, ka MYE scheme ka pynthikna ban pynmih haduh 3 lak ki jait Kam ha kine ki 5 snem ban wan bad ban pynioh T.1000 shi bnai (T.12,000 shi snem) ia baroh ki samla ki bym pat ioh kam kiba hap hapoh ka rta 21-40 snem.
';
$lang['faq5-q'] = 'Hato hap ban leh eiei lane pyndep kano kano ka kam man ka bnai ban ioh ia kane ka pisa?
';
$lang['faq5-a'] = 'Em kane ka pisa yn pynpoi beit  sha ki bank account kum ka lad jingiarap ia man ki samla bym pat ioh kam ha ka jylla kiba hap hapoh ka rta 21-40 snem.
';
$lang['faq6-q'] = 'Hato ki samla ba lah dep register ki don kam ban don ka bank account ban ioh ia kine ki jingiarap?
';
$lang['faq6-a'] = 'Lada phim don ka bank account ruh, phi lah ban ioh ia kane ka jingiarap. Ynda ka Meghalaya TMC ka lah ioh ban thaw sorkar ha u 2023, ki nongpyniaid kin iarap ia phi kumno ban ioh ia kane ka pisa, lyngba ki details ba lah register.
';
$lang['faq7-q'] = 'Shubuh ia ka MYE Card lah kum buh bakla ne jah?';
$lang['faq7-a'] = 'Ka jingkyrpad ka long ban buh bha ia kane ka card, tangba ngi don ia u Unique Identification Number (UID) jong baroh kito ki ba lah register lyngba ka sms lane online,ba lah ban pynshna thymmai biang ia ka card hadien habud
';

$lang['faq8-q'] = 'Lano kiba lah apply kin sdang ioh ia ki jingmyntoi?';
$lang['faq8-a'] = 'Da ka jingkyrkhu ki paidbah, lada shen ka TMC ka thaw ia ka sorkar ha Meghalaya kan sa pyntreikam ia ka scheme bad baroh ki samla bym pat don kam ha ka jylla kiba hap hapoh ka rta 21-40 snem kin sdang ban ioh ia kine ki jingmyntoi';
$lang['faq9-q'] = 'Hato nga donkam ban ai ia ki identification proof ha ka por ba pynrung kyrteng?';
$lang['faq9-a'] = 'Em, phim donkam ban ai kano kano ka identification proof ha ka por ba pynrung kyrteng.';
$lang['faq9-a-1'] = 'a)	Pyndap ia ka form pynrung kyrteng na ka bynta ka WE Card da kaba ai ia ki jingtip bniah jong kano kano ka kynthei na ka longïing. ';
$lang['faq9-a-2'] = 'b ) Shim ia ka smart kit kaba don  ia ka card bad ka UID. Ka UID kadei ki 11 digit alpha-numeric code kaba shon shapoh sha kadiang hakhmat jong ka card. (Nuksa: GLC 1234 5678)  ';
$lang['faq9-a-3'] = ' C ).Pynskhem da kaba sakhi ia ka card da kaba phah SMS sha 96877 96877 bad <Unique Identification Number >, <kyrteng jong ki nongpynrung kyrteng>. Mynta phi lah pynrung kyrteng da kaba jop na ka bynta ka card';
$lang['faq10-q'] = 'Hato Ngan ioh jingmyntoi na ka MYE Scheme tang lada nga mynjur ban rung sha ka party jong phi? ';
$lang['faq10-a'] = 'Em, kino kino ki samla ki bym pat don kam  bad kiba dei ruh ki nongshong shnong jong ka Meghalaya kiba haka rta 21-40 snem, ki lah ban pynrung kyrteng na ka bynta ka card, khlem da peit ia ki jingngeit politik jong ki lane ka rai ban rung sha ka party. 
';

$lang['faq11-q'] = 'Ha kano ka rukom ban pyndep ia ka jingsiew? Hato kan dei shisien shi snem? ';
$lang['faq11-a'] = 'Em, ka dor kumba lah buh yn ai man la u bnai. Te ki baroh samla kiba lah dep pynrung kyrteng kiba hapoh ka rta 21-40 snem kin ioh beit ia kane ka pisa kaba kaba  T. 1000 shi bnai (T.12000 shi snem) ';
$lang['faq12-q'] = ' Hato Ngan ioh ia ka pisa ha kti?';
$lang['faq12-a'] = 'Em, ngin pynpoi beit sha ki bank account jong phi.';
$lang['faq13-q'] = 'Khlem ki jingtip ba bniah kum ki bank account number, ki identification card, kumno ka party kan lah ban mang tyngka na bynta kane?';
$lang['faq13-a'] = ' Ka party kan ym pyntreikam ia ka scheme. Da ka jingkyrkhu jong ki paidbah, ynda ka Meghalaya TMC ka lah thaw Sorkar, yn sa pyn treikam ia ka scheme. Ki nong pyniaid jong ka Sorkar kin sa wan mih sha ki nong pynrung kyrteng katkum ka jingtip ba bniah ba ai mynta.
';
$lang['faq14-q'] = 'Lada jia ba une u PIN ba ngi ioh u dei u ba shukor bad ki bank account jong ngi kin shaphriang? ';
$lang['faq14-a'] = 'Em, iaki bank account jong phi yn ym lum, kumta ym don lad iaki bank account ban saphriang. U PIN uba phi ïoh u dei tang na ka bynta ban pynskhem iaka MYE Card. Ka Meghalaya TMC kam phone ban pan ia ki bank details jong phi, sngewbha wan ujor lada don ki ba leh kumta. 

';
$lang['faq15-q'] = 'Hato ki jingmyntoi ba ngi ioh na ki scheme ba mynta yn duh noh lada ngi pynrung kyrteng ha ka MYE Scheme?
';
$lang['faq15-a'] = 'Baroh ki scheme kiba dei hok jong u paidbah ka Meghalaya kin dang iai bteng.';
$lang['faq16-q'] = 'Lada ym Don kynthei ba lah San ha ka ing, lah ba ngin pynrung kyrteng bad ioh ia ki jingmyntoi jong ka scheme? ';
$lang['faq16-a'] = ' Kum kita ki jingkyrpad ngin sa pyrkhat kat kum ka nongrim.';
$lang['faq17-q'] = 'Ka don website? Lah ba ngin pynrung kyrteng online?  ';
$lang['faq17-a'] = 'Hooid, phi lah ban pynrung kyrteng online na ka bynta ka scheme.  Ka website URL ka dei <a href="https://www.tmcwecard.com/">www.tmcwecard.com</a>.';
$lang['faq18-q'] = 'Lada jia ba kane ka PIN ka ba ngi ioh kadei kaba shukor bad ki bank account jong ngi kin saphriang ?';
$lang['faq18-a'] = 'Em, iaki bank account jong phi yn ym lum, kumta ym don lad iaki bank account ban saphriang. U PIN uba phi ïoh u dei tang na ka bynta ban pynskhem iaka WE Card. Ka Meghalaya TMC kam phone ban pan iaka bank details, sngewbha wan ujor lada don ki ba leh kumta.';
$lang['faq19-q'] = 'Hato ka scheme kaba ngi ioh mynta lane ka pension ngin duh lada ngi pynrung kyrteng ha kane ka MFI WE Scheme? ';
$lang['faq19-a'] = ' Baroh ki scheme ki ba ki briew ki dei ban ioh ha Meghalaya kin dang iai bteng.';

/**registration form */
$lang['provide_details'] = 'Pyndap iaki jingkylli ba donkam';
$lang['female_details'] = "Ki jingtip bniah shaphang i kynthei rangbah jong ka longiing";
$lang['reg_tmc_mem'] = 'Ka Registration form jongka MYE Card';
$lang['video_guide_header'] = 'Registration Guide';
$lang['reg_phone'] = 'Pyndap ia u mobile number';
$lang['reg_phone_ph'] = 'Pyndap ia u mobile number ba pura';
$lang['reg_begin'] = 'Sdang iaka registration';
$lang['how_to_how'] = 'How to Register?';
$lang['enter_mob_for_reg'] = 'Step 1: Enter Mobile Number to begin registration';
$lang['enter_pin'] = 'Step 2: Enter OTP to confirm your mobile number ';
$lang['fill_details'] = 'Step 3: Fill the required details';
$lang['get_your_card'] = 'Step 4: Get your <span class="yellow">Griha Laxmi Card</span>';
$lang['know_more'] = '<i class="text-white">To Know more click <a id="faq" class="yellow" target="_blank" href="faq/faq.html"><u>FAQs</u></a>  or Call us on </i> <b class="ph_yellow">97620 97620</b> ';

/**Enter OTP form */
$lang['OTP_verify'] = 'Pyntikna ia u OTP';
$lang['digit_text'] = 'Lah phah ia u otp ha u mobile number phi  <span id="otp_phone_no"></span> lyngba ka sms bad ka ivr';
$lang['reg_otp_enter'] = 'Pyndap ia u OTP';
$lang['reg_otp_generate'] = 'Pynmih OTP';
$lang['reg_can_gen_otp'] = '<div class="timer_div" style="display: none;">Phi lah ban pynmih biang ia u OTP hadein   <span id="timer"></span> sekhons</div>';
$lang['otp_submit_btn'] = 'Pyndap noh';

/**personal details */
$lang['form_heading'] = 'Ka form online jong ka WE card';
$lang['name_label'] = 'Kyrteng ';
$lang['placeholder_name'] = 'Ka kyrteng pura';
$lang['phone'] = 'U mobile number';
$lang['placeholder_ph'] = 'Your Mobile Number';
$lang['gender'] = 'U/Ka';
$lang['gender_male'] = 'Shynrang';
$lang['gender_female'] = 'Kynthei';
$lang['gender_other'] = 'Kiwei';
$lang['female_head'] = 'Phi dei ka longkmie ka longing?';
$lang['yes'] = 'Haoid ';
$lang['no'] = 'Em ';
$lang['female_head_name'] = 'Kyrteng jong i kynthei rangbah jong ka longing';
$lang['female_head_name_ph'] = 'Pyndap Kyrteng jong i kynthei rangbah jong ka longing';
$lang['father_name'] = 'Father’s/Husband’s Name';
$lang['father_placeholder'] = 'Your Father’s/Husband’s Name';
$lang['age'] = 'Age of Female Head';
$lang['select_age'] = 'Select Age';
$lang['Female_head_phone'] = "Phone Number";
$lang['Female_head_phone_ph'] = "Enter phone number";
$lang['whatsapp'] = 'Whatsapp Number';
$lang['whatsapp_ph'] = 'Enter Whatsapp Number';
$lang['bank_account'] = 'Do you have a bank account?';
$lang['adress'] = 'Ai ïa ka jaka sah jong phi.';
$lang['adress_ph'] = 'Ai ïa ka jaka sah jong phi.';
$lang['pincode'] = 'Pin Code';
$lang['pincode_ph'] = 'Enter Pincode';
$lang['ward'] = 'Village Name/Ward Number';
$lang['ward_ph'] = 'Enter Village Name/Ward Number';
$lang['label_district'] = 'Ka district ';
$lang['select_district'] = 'Pyndap iaka district ';
$lang['label_ac'] = 'ka ac';
$lang['select_ac'] = 'Pyndap ka ac';
$lang['label_block'] = 'Block/Taluka';
$lang['select_block'] = 'Select Block/Taluka';
$lang['glc_card'] = 'Phi lah ioh iaka MYE Card?';
$lang['glc_cardno'] = 'Pyndap ia u Identification number (UID) badon haka MYE Card';
$lang['glc_cardno_ph'] = 'Pyndap ia u UID';
$lang['terms_and_condition'] = 'Nga <a href="' . base_url() . 'terms-and-conditions.html" target="_blank"><u>monjur ia kita ki kyndon bad ki jingiakut. </u></a>';
$lang['submit_btn'] = 'Pyndap noh';
$lang['dob'] = 'Ka sngi kha';
$lang['dob_year'] = 'Snem ';
$lang['dob_month'] = 'Bnai';
$lang['dob_month_1'] = 'Kyllalyngkot';
$lang['dob_month_2'] = 'Rymphang';
$lang['dob_month_3'] = 'lber';
$lang['dob_month_4'] = 'Iaïong';
$lang['dob_month_5'] = 'Jymmang';
$lang['dob_month_6'] = 'Jylliew';
$lang['dob_month_7'] = 'Naitung';
$lang['dob_month_8'] = 'Nailar';
$lang['dob_month_9'] = 'Nailur';
$lang['dob_month_10'] = 'Risaw';
$lang['dob_month_11'] = 'Naiwieng';
$lang['dob_month_12'] = 'Nohprah';
$lang['dob_day'] = 'Sngi';
$lang['mandatory_fields'] = '* - Ki jaka pyndap ba donkam';
$lang['age'] = 'Age';
$lang['age_ph'] = 'Pyndap iaka kyrta jongphi';
/** Footer Section */
$lang['copyright'] = 'Copyright © 2023 Meghalaya Trinamool Congress';
$lang['follow_text'] = 'Sa bud iangi ha:';
$lang['faq'] = 'FAQs';
$lang['faq_link'] = base_url() . 'faq.html';
$lang['tc'] = 'Terms of use';
$lang['privacy'] = 'Privacy Policy';
