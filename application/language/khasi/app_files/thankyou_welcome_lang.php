<?php
defined('BASEPATH') or exit('No direct script access allowed');

/** Thank you page */
$lang['welcome_to_tmc'] = 'Khublei [NAME]. ka MYE card jongphi ka lah long ban pyndonkam';
$lang['discalmer'] = 'Kane ka card lah pynmih halor ki jingpyntip ba maphi por ba pyndap iaka form. lada shem ba ai jingpyntip bym biang, ka party ka lah ban shim jingkylli bad pynshitom katkum ki aiñ ka party';
$lang['thank_download_btn'] = 'Download ia ka card';
$lang['thank_download_welcome'] = 'Download iaka Welcome Letter';
$lang['thank_review_btn'] = 'Pule bha iaki aiñ jong ka scheme';
$lang['thank_signup_btn'] = 'Pyndap';
$lang['thank_front_view'] = 'Na khmat';
$lang['thank_back_view'] = 'Na dein';
$lang['thank_join_watsapp'] = 'Join ia kane ka whatsapp group';
$lang['thank_click_to_connect'] = 'Ban ioh ia ki message na ka Mamata Banerjee. snohkti ha ka whatsapp';
$lang['thank_join_btn'] = 'Bteng';
$lang['reg_date'] = 'Ka sngi registration';

/** pop up  */
$lang['modal_text'] = 'गोंयच्या तरणाट्यांच्यां सपनांक आकार दिवपाक गोवा टीएमसीन "यूवा शक्ति कार्ड" लाँच केलां.गोंयच्या तरणाटयांक ४% व्याजदरान २० लाखांचे क्रेडिट कार्ड (१८-४५ वर्सां)';
$lang['modal_button'] = 'आतां नोंदणी करात';

/** Footer Section */
$lang['copyright'] = 'Copyright © 2023 Meghalaya Trinamool Congress';
$lang['follow_text'] = 'Follow us on:';
$lang['faq'] = 'FAQs';
$lang['tc'] = 'Terms of use';
$lang['privacy'] = 'Privacy Policy';
