<?php

defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller']            = 'welcome';
$route['signup']                        = 'welcome/signup';
$route['signup/kha']                    = 'welcome/signup';
$route['signup/garo']                   = 'welcome/signup';
$route['volunteersignup']              = 'welcome/volunteersignup';
$route['volunteersignup/kha']          = 'welcome/volunteersignup';
$route['volunteersignup/garo']         = 'welcome/volunteersignup';
$route['thankyou']              = 'welcome/thankyou';
$route['thankyou/kha']          = 'welcome/thankyou';
$route['thankyou/garo']           = 'welcome/thankyou';
$route['download']              = 'welcome/download';
$route['404_override']          = 'findcontent';
$route['translate_uri_dashes']  = TRUE;
$route['^(\w{2})$']             = $route['default_controller'];
$route['^(\w{3})$']             = $route['default_controller'];
$route['^(\w{4})$']             = $route['default_controller'];
