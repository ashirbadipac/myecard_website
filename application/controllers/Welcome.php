<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Website_model');
        $this->load->model('Common_model');
        $this->load->helper('url');
        date_default_timezone_set('Asia/Kolkata');
        $this->load->library('session');
    }

    public function index()
    {
        $data['intro_video'] = 'https://youtu.be/3Jc0FSRzN7A';
        $data['landing_video'] = 'https://www.youtube.com/embed/6NBDWsSIcKU?&autoplay=1&rel=0&fs=0&showinfo=0&autohide=3&modestbranding=1';
        $data['landing_video_kok'] = 'https://www.youtube.com/embed/5N2bglaG744?&autoplay=1&rel=0&fs=0&showinfo=0&autohide=3&modestbranding=1';
        $data['landing_video_mr'] = 'https://www.youtube.com/embed/6NBDWsSIcKU?&autoplay=1&rel=0&fs=0&showinfo=0&autohide=3&modestbranding=1';
        //$total_supporter = $this->Website_model->fetch_data('website', 'total_supported');
        // echo '<pre>';print_r($total_supporter);exit;
        // $data['total_supporter'] = $total_supporter[0]['total_supported'];
        $whereUser = array('where' => array('phone_no !=' => ''));
        $gettotal = $this->Website_model->fetch_count('tbl_card_details', $whereUser);
        $data['total_supporter'] = $gettotal;
        $this->render('public/homepage_view', $data);
    }

    public function signup()
    {
        $where = array('order_by' => ['district' => 'asc']);
        $data['district_lists'] = $this->Website_model->fetch_data('meghalaya_district_ac', 'district,district_code',  $where, false, true);

        $this->render('public/register_view', $data, 'register_master');
    }
    public function volunteersignup()
    {
        $where = array('order_by' => ['district' => 'asc']);
        $data['district_lists'] = $this->Website_model->fetch_data('meghalaya_district_ac', 'district,district_code',  $where, false, true);

        $this->render('public/volunteerregister_view', $data, 'volunteerregister_master');
    }
    public function getAC()
    {
        $district = $_POST['district'];
        $where = array('where' => ['district' => $district], 'order_by' => ['ac' => 'asc']);
        $result = $this->Website_model->fetch_data('meghalaya_district_ac', 'ac,ac_code', $where);
        echo json_encode($result);
        exit;
    }

    public function thankyou()
    {
        $uid = isset($_GET['uid']) ? $_GET['uid'] : '';
        if (!empty($uid)) {
            $phone_no = $this->Common_model->encrypt_decrypt('decrypt', $uid, $this->Common_model->secret_key);
        }
        if (!isset($phone_no)) {
            redirect('/', 'refresh');
        }

        $whereUser = array('or_where' => array('card.phone_no' => $phone_no, 'web.phone_no' => $phone_no));
        $whereUser['left_join'] = array('web_registration AS web' => "web.phone_no = card.phone_no");
        $getUserDetails = $this->Website_model->fetch_data('tbl_card_details AS card', 'card.*,web.user_name,web.ac_name,web.age', $whereUser, true);

        if (empty($getUserDetails)) {
            redirect('/', 'refresh');
        }

        $response['user'] = $getUserDetails;
        $custom_string  = str_replace('MYEO', '', $getUserDetails['card_unique_id']);
        $custom_string  = str_replace('MYE', '', $custom_string);
        $card_format    = strpos($getUserDetails['card_unique_id'], 'MYEO') !== false ? 'MYEO ' : 'MYE ';
        $card_format    .= join(" ", str_split($custom_string, 4));
        $response['user']['card_format'] = $card_format;


        $response['user']['encrypted_phone_no'] = $this->Common_model->encrypt_decrypt('encrypt', $phone_no, $this->Common_model->secret_key);
        $this->render('public/thankyou_view', $response, 'thankyou_master');
    }

    public function sendOTP()
    {
        /** Parameters */
        $currentTime = date("Y-m-d H:i:s");
        $ip = $_SERVER["REMOTE_ADDR"];
        $language = trim($_POST['language']);
        $otp = $this->Common_model->getOTPText(5);
        $response = array();

        $phone_no = $_POST['phone_no'];
        $data = array(
            'phone_no' => $phone_no,
            'otp' => $otp,
            'submitted_timestamp' => $currentTime,
            'ip_address' => $ip,
        );

        /** Check whether mobile number is valid or not */
        if (preg_match('/^[0-9]+$/', $phone_no) && (strlen($phone_no) == 10)) {

            // $whereOTP   = array('where' => array('ip_address' => $ip));
            // $checkCount = $this->Website_model->fetch_count('user_otp', $whereOTP);

            // if ($checkCount >= 50) {
            //     $response['status']     = 'NOK';
            //     $response['message']    = $language == "en" ? "You have exceeded the maximum attempts. Please try after 3 hours" : ($language == "kok" ? "तुमी थारयल्या परस चड फावटीं  यत्न केले ,तीन वरांनी परत प्रयत्न करात" : "तुम्ही कमाल प्रयत्न ओलांडले आहेत. कृपया 3 तासांनंतर प्रयत्न करा");
            //     echo json_encode($response);
            //     exit;
            // }

            /** Check whether OTP already generated or not */
            $where = array('where' => array('phone_no' => $phone_no));
            $getOTPDetails = $this->Website_model->fetch_data('user_otp', '*', $where, true);
            if (empty($getOTPDetails)) {
                $this->Website_model->insert_single('user_otp', $data);
            } else {
                $otp = substr($getOTPDetails['otp'], 0, 5);
                $data['otp'] = $otp;
                $seconds = abs(strtotime($getOTPDetails['submitted_timestamp']) - time());
                if ($seconds < 90) {
                    $response['status'] = 'NOK';
                    $response['message'] = $language == "en" ? "You can regenerate the OTP in 90 seconds" : ($language == "kha" ? "Phi lah ban ioh biang ia u otp hadein 90 sekhon " : "90 second ong·o OTPko bikotaina man·gen");
                    echo json_encode($response);
                    exit;
                }
                $whereUpdate = array('where' => array('phone_no' => $phone_no));
                $this->Website_model->update_single('user_otp', $data, $whereUpdate);
            }

            if ($language == 'kha') {
                $msg = "गृहलक्ष्मी कार्डान नोंदणी सुरू करपाक OTP $otp हो आसा\n\nGOA TMC";
                $tid = '1707163912736700997';
                $this->Common_model->sendSMS($phone_no, $msg, $tid, true);
            } else {
                $msg = "$otp is your OTP to begin registration for Griha Lakshmi Card.\n\nGOA TMC";
                $tid = '1707163906969764151';
                $this->Common_model->sendSMS($phone_no, $msg, $tid);
            }

            /** SEND OTP Via IVR */
            // $campaign_name = "otp_ivr2";
            // $campaign_id = "203";
            // $ivr_link = "http://103.91.86.18:8249/zobitel/admin/core/OBD/OBD_API_CALL.php?Campaignid=$campaign_id&Campaign_name=$campaign_name&Calling_Number=$phone_no&VALUE=$otp";
            // $this->Common_model->sendIVR($ivr_link);

            // $data = array(
            //     'phone_no' => $phone_no,
            //     'card_unique_id' => '',
            //     'user_name' => '',
            //     'campaign_name' => $campaign_name,
            //     'campaign_id' => $campaign_id,
            //     'ivr_link' => $ivr_link,
            //     'submitted_date' => $currentTime,
            // );
            // $this->Website_model->insert_single('tbl_ivr_log', $data);
            /** END:: SEND OTP Via IVR */

            $response['status'] = 'OK';
        } else {
            $response['status'] = 'NOK';
            $response['message'] = $language == "en" ? "Please enter valid mobile number" : ($language == "kha" ? "Ai u number phone ba tikna" : "Ka·sapae chong·motgipa mobile numberko gapatbo");
        }

        echo json_encode($response);
        exit;
    }

    public function verifyOTP()
    {
        /** Parameters */
        $currentTime = date("Y-m-d H:i:s");
        $phone_no = trim($_POST['phone_no']);
        $otp = trim($_POST['otp']);
        $language = trim($_POST['language']);
        $referral_code = trim($_POST['referral_code']);
        $response['is_exist'] = false;
        $where = array('where' => array('phone_no' => $phone_no));
        $getOTPDetails = $this->Website_model->fetch_data('user_otp', ['otp', 'submitted_timestamp', 'total_attempts', 'last_attempt_timestamp', 'retouch_refer_by'], $where, true);

        if ($getOTPDetails['total_attempts'] >= 5) {
            $hourdiff = round((strtotime($currentTime) - strtotime($getOTPDetails['last_attempt_timestamp'])) / 3600, 1);
            if ($hourdiff < 3) {
                $response['status'] = 'NOK';
                $response['message'] = $language == "en" ? "You have exceeded the maximum attempts. Please try after 3 hours" : ($language == "kha" ? "Phi lah pyrshang palat iaka ei ba shah. Pyrshang hadein 3 kynta" : "Konta 3 ja·man");
                echo json_encode($response);
                exit;
            }
        }

        if (empty($getOTPDetails) || $getOTPDetails['otp'] != $otp) {
            $updateData = array(
                'total_attempts' => $getOTPDetails['total_attempts'] + 1,
                'last_attempt_timestamp' => $currentTime,
            );
            $whereUpdate = array('where' => array('phone_no' => $phone_no));
            $this->Website_model->update_single('user_otp', $updateData, $whereUpdate);

            $response['status'] = 'NOK';
            $response['message'] = $language == "en" ? "Invalid OTP" : ($language == "kha" ? "Bakla OTP" : "Ong·gijagipa OTP");
            echo json_encode($response);
            exit;
        }

        $minutes = abs(strtotime($getOTPDetails['submitted_timestamp']) - time()) / 60;

        if ($minutes > 5) {
            $response['status'] = 'NOK';
            $response['message'] = $language == "en" ? "OTP has been expired" : ($language == "kha" ? "OTP ym treikam shuh" : "OTP cholijaha");
            echo json_encode($response);
            exit;
        }

        $data = array('is_verified' => 1);
        $this->session->set_userdata(['is_verified' => true]);

        /* checking phone already exists in card details */
        $whereUser = array('where' => array('phone_no' => $phone_no));
        $getCount = $this->Website_model->fetch_count('tbl_card_details', $whereUser);
        if ($getCount > 0) {
            $response['guid'] = $this->Common_model->encrypt_decrypt('encrypt', $phone_no, $this->Common_model->secret_key);
            $response['is_exist'] = true;
        }

        // /** checking phone already exists in web registration */
        if ($response['is_exist'] == false) {
            $whereUser = array('or_where' => array('phone_no' => $phone_no, 'female_head_phone_number' => $phone_no));
            $getUserDetails = $this->Website_model->fetch_data('web_registration', '*', $whereUser, true);

            if (!empty($getUserDetails)) {
                $female_head_phone_number = $getUserDetails['female_head_phone_number'];
                $response['guid'] = $this->Common_model->encrypt_decrypt('encrypt', $female_head_phone_number, $this->Common_model->secret_key);
                $response['is_exist'] = true;
            }
        }

        // /** Retouch Log */
        $response['retouch'] = false;
        if ($response['is_exist'] == true && !empty($referral_code)) {
            $retouchLog = array(
                'phone_no' => $phone_no,
                'retouch_refer_by' => $referral_code,
                'submitted_date' => $currentTime,
            );
            $this->Website_model->insert_single('tbl_retouch_log', $retouchLog);
            if (empty($getOTPDetails['retouch_refer_by'])) {
                $data['retouch_refer_by'] = $referral_code;
            } else {
                $response['retouch'] = true;
            }
        }
        // /** Retouch Log End */

        $this->Website_model->update_single('user_otp', $data, $where);
        $response['lang'] = $language;
        $response['status'] = 'OK';
        echo json_encode($response);
        exit;
    }

    public function registration()
    {
        /** Parameters */

        $response = array();
        //$is_verified = $this->session->userdata('is_verified');
        $currentDate = date("Y-m-d H:i:s");
        $ip = $_SERVER["REMOTE_ADDR"];
        $phone_no = trim($_POST['read_only_phone_no']);
        // $otp = $_POST['otp'];
        $language = trim($_POST['language']);
        $name = trim($_POST['name']);
        $father_name = trim($_POST['father_name']);
        $gender = trim($_POST['gender']);
        //$dob = "";
        $age = isset($_POST['age']) ? $_POST['age'] : '';
        $whatsapp_number = trim($_POST['whatsapp_number']);
        $is_glc_card = trim($_POST['is_glc_card']);
        $glc_cardno = trim($_POST['glc_cardno']);
        $ac_name = $_POST['ac'];
        $ac_no = trim($_POST['ac_no']);
        $district_name = trim($_POST['district']);
        $district_code = trim($_POST['district_code']);
        $refered_by = trim($_POST['referral_code']);
        // if ($_POST['regDay'] != "" && $_POST['regMonth'] != "" && $_POST['regYear'] != "") {
        //     $dob = $_POST['regDay'] . "-" . $_POST['regMonth'] . "-" . $_POST['regYear'];
        //     $today = date("Y-m-d");
        //     $diff = date_diff(date_create($dob), date_create($today));
        //     $age = $diff->format('%y');
        //     $dob = $_POST['regYear'] . "-" . $_POST['regMonth'] . "-" . $_POST['regDay'];
        // }
        // $is_female_head = trim($_POST['femalehead']);
        // $female_head_name = $is_female_head == 'Yes' ? $name : trim($_POST['femaleheadname']);
        // $female_head_phone_number =  $phone_no;

        // $is_bank_account         = $_POST['bankaccount'];
        // $address = trim($_POST['address']);
        // $pincode                    = trim($_POST['pincode']);

        // $village_name_ward_no       = trim($_POST['village_name']);

        // $block_name                 = $_POST['block'];
        $referral_code = $this->Common_model->getReferral(2) . substr(str_shuffle($phone_no), -6) . $this->Common_model->getReferral(2);

        $data = array(
            'phone_no' => $phone_no,
            'user_name' => $name,
            'gender' => $gender,
            'age' => $age,
            'whatsapp_number' => $whatsapp_number,
            'district_name' => $district_name,
            'ac_name' => $ac_name,
            'ip_address' => $ip,
            'registration_date' => $currentDate,
            'language' => $language,
            'referral_code' => $referral_code,
            'refered_by' => $refered_by,
            // 'is_female_head' => $is_female_head,
            // 'female_head_name' => $female_head_name,
            // 'female_head_phone_number' => $female_head_phone_number,
            // 'is_bank_account'        => $is_bank_account,
            // 'address' => $address,
            // 'pincode'                   => $pincode,
            // 'village_name_ward_no'      => $village_name_ward_no,
            // 'block_name'                => $block_name,

        );

        /** Check wheather phone number is correct or not */
        if (empty($phone_no)) {
            $response['status'] = 'NOK';
            $response['message'] = $language == "en" ? "Please enter valid mobile number" : ($language == "kha" ? "Ai u number phone ba tikna" : "Kasapae nangni Mobile Number ko onbo");
            echo json_encode($response);
            exit;
        }

        /** Check wheather phone number is verified or not */
        // if (!$is_verified) {
        //     $response['status'] = 'NOK';
        //     $response['message'] = $language == "en" ? "Mobile number is not verified" : ($language == "kha" ? "Pat tikna ia u phone number" : "Mobile numberko ong·ama ong·ja nikuja");
        //     echo json_encode($response);
        //     exit;
        // }

        /** Checking whether mobile number exist or not in card details */
        $whereUser = array('where' => array('phone_no' => $phone_no));
        $getCardDetails = $this->Website_model->fetch_data('tbl_card_details', '*', $whereUser, true);
        if (!empty($getCardDetails)) {
            $card_phone_no = $getCardDetails['phone_no'];
            $response['message'] = "$phone_no is already registered for the MYE Card. Please use a different phone number.";
            if ($card_phone_no == $phone_no) {
                $response['message'] = "$phone_no is already registered for the MYE Card. Please use a different phone number.";
            }
            $response['status'] = 'NOK';
            echo json_encode($response);
            exit;
        }

        /** Checking whether mobile number exist or not in web registration */
        $whereUser = array('where' => array('phone_no' => $phone_no));
        $getUserDetails = $this->Website_model->fetch_data('web_registration', '*', $whereUser, true);
        if (!empty($getUserDetails)) {
            $user_female_head_phone_number = $getUserDetails['phone_no'];
            $response['message'] = "$phone_no is already registered for the MYE Card. Please use a different phone number.";
            if ($user_female_head_phone_number == $phone_no) {
                $response['message'] = "$phone_no is already registered for the MYE Card. Please use a different phone number.";
            }
            $response['status'] = 'NOK';
            echo json_encode($response);
            exit;
        }

        /** Check wheather card number exist or not */
        if ($is_glc_card == "Yes") {
            $glc_cardno = str_replace(' ', '', $glc_cardno);
            $glc_cardno = strtoupper($glc_cardno);
            $whereCard = array('where' => array('card_unique_id' => $glc_cardno));
            $checkCardNo = $this->Website_model->fetch_data('tbl_card_details', '*', $whereCard, true);
            if (empty($checkCardNo)) {
                $response['message'] = $language == "en" ? "Please enter valid card number" : ($language == "kha" ? "Ai ka card number ba biang" : "Ka·sapae chong·motgipa card numberko gapatbo");
                $response['status'] = 'NOK';
                echo json_encode($response);
                exit;
            } else {
                $checkCardNo_phone_no = $checkCardNo['phone_no'];
                $checkCardNo_pin = $checkCardNo['card_pin'];
                if (!empty($checkCardNo_phone_no)) {
                    $response['message'] = $language == "en" ? "$glc_cardno is already registered with a different phone number. Enter a valid Card Number." : ($language == "kha" ? "$glc_cardno Lah dep register da uwei number. sngewbha ai da uwei number phone" : "$glc_cardno Chong·motgipa Card Number");
                    $response['status'] = 'NOK';
                    echo json_encode($response);
                    exit;
                }
            }
            $data['is_glc_card'] = $is_glc_card;
            $data['glc_cardno'] = $glc_cardno;
        }

        /** Insert data in web_registration form */
        $volunteer_id = $this->Website_model->insert_single('web_registration', $data);
        $ref_category = '';
        if ($is_glc_card == "Yes") {
            $unique_card_no = $glc_cardno;
            $pin = $checkCardNo_pin;
            $ref_category = 'Field wCard';

            $data = array(
                'phone_no' => $phone_no,
                'user_name' => $name,
                'registration_type' => 'online',
                'is_referral' => '1',
                'referral_category' => $ref_category,
                'registration_date' => $currentDate,
            );
            $this->Website_model->update_single('tbl_card_details', $data, $whereCard);
        } else {
            $lang_code = $language == 'kha' ? '03' : ($language == 'garo' ? '02' : '01');
            $serial_no = 000000;
            //$unique_card_no = 'WECO' . $district_code . str_pad($ac_no, 2, '0', STR_PAD_LEFT)  . $lang_code . ($serial_no + $volunteer_id);
            $unique_card_no = 'MYEO' . $lang_code . str_pad($volunteer_id, 6, '0', STR_PAD_LEFT);
            $pin = $this->Common_model->getOTPText(4);

            $data = array(
                'phone_no' => $phone_no,
                'card_unique_id' => $unique_card_no,
                'card_pin' => $pin,
                'user_name' => $name,
                'registration_type' => 'online',
                'registration_date' => $currentDate,
            );
            if ($refered_by != '') {
                $whereRef = array('where' => array('referral_code' => $refered_by));
                $getReferralDetails = $this->Website_model->fetch_data('tbl_volunteers_referral_code', '*', $whereRef, true);
                $data['is_referral'] = '1';
                $data['referral_category'] = !empty($getReferralDetails) ? $getReferralDetails['category'] : 'Digital';
                $ref_category = $data['referral_category'];
            }
            $this->Website_model->insert_single('tbl_card_details', $data);
            // UPDATE TOTAL SUPPORTER

            $this->db->set('total_supported', 'total_supported+1', FALSE);
            $this->db->where('id', 1);
            $this->db->update('website');
        }

        if (!empty($ref_category)) {
            $whereRefCat = array('where' => array('id' => $volunteer_id));
            $this->Website_model->update_single('web_registration', ['referral_category' => $ref_category], $whereRefCat);
        }

        /** SUCCESS SMS */
        $success_msg = "Hi $name, Congratulations! MYE CARD: $unique_card_no is registered. Your PIN is $pin. Upon TMC forming the Govt., as per MYE Scheme, 3 lakh jobs will be created over the next 5 years and an allowance of ₹1,000 monthly will be provided to all unemployed youth. To avail benefit, Support TMC.\n-Meghalaya TMC";
        $tid = '1707167247144098029';
        $this->Common_model->sendSMS($phone_no, $success_msg, $tid, true);

        /** WHATSAPP SMS */
        // $whatsapplink = "https://www.tmcwecard.com/joinwhatsapp/" . $ac_name;
        // $tiny_whatsapp_URL = $this->get_tiny_url($whatsapplink);
        // $whatsapp_link_msg = "Hi $name,\nStay tuned with us as we bring more updates on the WE Card. Join the official WhatsApp group $tiny_whatsapp_URL .\n\n-Meghalaya TMC";
        // $whatsapp_tid = '1707167049545985682';
        // $this->Common_model->sendSMS($phone_no, $whatsapp_link_msg, $whatsapp_tid, true);

        if ($language == "kha") {
            $msg2 =  "Khublei $name, Ngi kitbok ia phi! MYE CARD:$unique_card_no lah dep register. U PIN jong phi dei $pin. Hadien ba lah thaw sorkar ka TMC, na ka MYE Scheme, yn sa pynmih 3 lak tylli ki Kam ha kine ki 5 snem ki ban sa wan bad yn ai bai bnai T.1000 shi bnai sha baroh ki samla ki bym pat ioh kam. Ban ioh ia kine ki jingmyntoi, kyrshan ia ka TMC.\n- Meghalaya TMC";
            $tid2 =  '1707167248957854587';
            $this->Common_model->sendSMS($phone_no, $msg2, $tid2, true);

            // $whatsapp_link_msg_2 = "Khublei ia phi $name,\nIaineh bad ngi, ngin dang wanrah shuh shuh ki jingtip kiba bniah shaphang ka WE Card. Rung sha ka official Whatsapp group jong ngi $tiny_whatsapp_URL.\n\n-Meghalaya TMC";
            // $whatsapp_tid_2 = '1707167068761718367';
            // $this->Common_model->sendSMS($phone_no, $whatsapp_link_msg_2, $whatsapp_tid_2, true);
        }

        if ($language == "garo") {

            $msg2 = "Salam $name,Nambejok! MYE CARD: $unique_card_no-ko segatman.aha. Nang.ni PIN ian $pin ong.a. TMC sorkari rikna man.ode, MYE Scheme ni gita re.baenggipa bilsi 5 ni gisepo lak 3 kamrangko nakatate on.gen aro gipin man.dapanirang jaanti tangka gong 1000 ko kam gri dongenggipa Chadamberangna on.gen. Ia namgniko man.na gita TMC na chakpabo.\n- Meghalaya TMC";
            $tid2 = '1707167248928690374';
            $this->Common_model->sendSMS($phone_no, $msg2, $tid2, true);

            // $whatsapp_link_msg_2 =  "Hi $name,\nChing baksa dongbo jedakode WE Card-ni bidingo gital koborrangko man•gen. Official WhatsApp group-o bak ra•bo $tiny_whatsapp_URL\n\n-Meghalaya TMC";
            // $whatsapp_tid_2 = '1707167068851228717';
            // $this->Common_model->sendSMS($phone_no, $whatsapp_link_msg_2, $whatsapp_tid_2, true);
        }

        if ($language == "garo") {
            $campaign_name = 'garoMY';
            $campaign_id = 107;
        } else  if ($language == "kha") {
            $campaign_name = 'khasiMY';
            $campaign_id = 106;
        } else {
            $campaign_name = 'enMY';
            $campaign_id = 105;
        }

        $ivr_link = "http://49.249.100.74:8234/zobitel/admin/core/OBD/OBD_API_CALL.php?Campaignid=$campaign_id&Campaign_name=$campaign_name&Calling_Number=$phone_no";

        $this->Common_model->sendIVR($ivr_link);

        // /*** SMS LOG START */
        $data = array(
            'phone_no' => $phone_no,
            'sms_text' => $success_msg,
            'scenario_type' => '1',
            'submitted_date' => $currentDate,
        );
        $this->Website_model->insert_single('tbl_outgoing_sms_log', $data);

        // $data = array(
        //     'phone_no' => $phone_no,
        //     'sms_text' => $whatsapp_link_msg,
        //     'submitted_date' => $currentDate,
        // );
        // $this->Website_model->insert_single('tbl_whatsapp_sms_log', $data);

        $data = array(
            'phone_no' => $phone_no,
            'card_unique_id' => $unique_card_no,
            'user_name' => $name,
            'campaign_name' => $campaign_name,
            'campaign_id' => $campaign_id,
            'ivr_link' => $ivr_link,
            'submitted_date' => $currentDate,
        );
        $this->Website_model->insert_single('tbl_ivr_log', $data);
        /** SMS LOG END */


        // $this->session->unset_userdata('is_verified');
        $response["status"] = 'OK';
        $response['guid'] = $this->Common_model->encrypt_decrypt('encrypt', $phone_no, $this->Common_model->secret_key);
        $response['lang'] = $language;
        echo json_encode($response);
        exit;
    }

    public function download()
    {
        $lang       = $_GET['lang'];
        $uid        = isset($_GET['uid']) ? $_GET['uid'] : '';
        $phone_no   = isset($_GET['phone']) ? $_GET['phone'] : '';
        if (!empty($uid) && empty($phone_no)) {
            $phone_no = $this->Common_model->encrypt_decrypt('decrypt', $uid, $this->Common_model->secret_key);
        }
        if (!isset($phone_no)) {
            redirect('/', 'refresh');
        }
        $whereUser = array('where' => array('phone_no' => $phone_no));
        $getUserDetails = $this->Website_model->fetch_data('tbl_card_details', '*', $whereUser, true);

        if (empty($getUserDetails)) {
            redirect('/', 'refresh');
        }
        $card_unique_id = $getUserDetails['card_unique_id'];
        $custom_string  = str_replace('MYEO', '', $getUserDetails['card_unique_id']);
        $custom_string  = str_replace('MYE', '', $custom_string);
        $card_format    = strpos($getUserDetails['card_unique_id'], 'MYEO') !== false ? 'MYEO ' : 'MYE ';
        $card_format    .= join(" ", str_split($custom_string, 4));
        $card_pin = $getUserDetails['card_pin'];
        $image_path = base_url() . 'assets/images/';
        $front = $lang == 'en' ? 'front.jpg' : ($lang == 'kha' ? 'front_kha.jpg' : 'front_garo.jpg');
        $back = $lang == 'en' ? 'back.jpg' : ($lang == 'kha' ? 'back_kha.jpg' : 'back_garo.jpg');
        $this->load->library('html2pdf_lib');
        $html = "";
        $html .= '<style>
                .card-content {
                    background-color:#181818;
                    position: absolute;
                    padding-right:3mm;
                    padding-left:3mm;
                    padding-top: 1.5mm;
                    padding-bottom: 1.5mm;
                    border-radius: 1mm;
                    text-align: center;
                    top:78%;
                    left:5%;
                }
                .card-content p {
                    font-size: 20px;
                    color: #ccc;
                    margin:0;
                    font-weight: bold;
                }
                .pin-content {
                    position: absolute;
                    text-align: center;
                    top:87%;
                    right:18%;
                    font-size:15px;
                    color:#000;
                }
        </style>';
        $html .= '<page format="83X130" orientation="L" backimg="' . $image_path . $front . '">';
        $html .= '<div class="card-content"><p>' . $card_format . '</p></div>';
        $html .= '</page>';
        $html .= '<page format="83X130" orientation="L" backimg="' . $image_path . $back . '">';
        $html .= '<div class="pin-content">' . $card_pin . '</div>';
        $html .= '</page>';
        $filename = $card_unique_id . '.pdf';
        $this->html2pdf_lib->converHtml2pdf($html, $filename);
    }


    //** Tiny Url */
    private function get_tiny_url($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url=' . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
