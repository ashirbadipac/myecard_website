<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cron extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Website_model');
        $this->load->model('Common_model');
        $this->load->helper('url');
        date_default_timezone_set('Asia/Kolkata');
        $this->load->library('session');
    }

    public function sendReferralSMS()
    {
        $sql = "SELECT u.name, u.referral_code, u.phone_no, u.language, u.is_refer25_sms_sent FROM user_registrations AS u WHERE is_refer25_sms_sent = 0 AND referral_code IN(SELECT t1.refer_by FROM user_registrations AS t1 WHERE refer_by != '' GROUP BY t1.refer_by HAVING COUNT(t1.refer_by) >= 25) LIMIT 100";
        $query = $this->db->query($sql);
        $referrals = $query->result_array();
        if (count($referrals) > 0) {
            $updateArray = [];
            foreach ($referrals as $ref) {
                if (!empty($ref['phone_no'])) {
                    $phone_no      = $ref['phone_no'];
                    $language      = $ref['language'];
                    $name          = $ref['name'];
                    $referral_code = $ref['referral_code'];
                    $sendNotifiUser[] = $phone_no;
                    $updateArray[] = array(
                        'phone_no' => $ref['phone_no'],
                        'is_refer25_sms_sent' => 1
                    );
                    $msg = "Congratulations! 25 new people have become TMC online members using your referral code " . $referral_code . ". Your contribution to the bright future of Goa is commendable.";
                    $this->Common_model->sendSMS($phone_no, $msg, $language);
                }
            }
            if (count($updateArray) > 0) {
                $this->db->update_batch('user_registrations', $updateArray, 'phone_no');
            }
        }
        $response['status'] = 'Ok';
        echo json_encode($response);
        exit;
    }

    public function sendIncompleteSMS()
    {
        // $sql = "SELECT `user_otp`.`phone_no`, `user_otp`.`submitted_timestamp`,`user_otp`.`ip_address` FROM `user_otp` WHERE `user_otp`.`is_verified` = 1 AND `user_otp`.`is_incomplete_sms_sent` = 0 AND `user_otp`.`submitted_timestamp` <= DATE_SUB(NOW(),INTERVAL 1 HOUR) AND `user_otp`.`phone_no` NOT IN (SELECT `user_registrations`.`phone_no` FROM `user_registrations`) ORDER BY `user_otp`.`id` ASC LIMIT 100";
        // $query = $this->db->query($sql);
        // $users = $query->result_array();
        // if (count($users) > 0) {
        //     $updateArray = [];
        //     foreach ($users as $user) {
        //         if (!empty($user['phone_no'])) {
        //             $phone_no = $user['phone_no'];
        //             $sendNotifiUser[] = $phone_no;
        //             $updateArray[] = array(
        //                 'phone_no' => $user['phone_no'],
        //                 'is_incomplete_sms_sent' => 1
        //             );
        //             $msg = "Hi, You are just a few steps away from becoming a TMC member. Please visit: # to complete your registration";
        //             // $this->Common_model->sendPromoSMS($phone_no, $msg, 'en');
        //         }
        //     }
        //     // if (count($updateArray) > 0) {
        //     //     $this->db->update_batch('user_otp', $updateArray, 'phone_no');
        //     // }
        // }
        $response['status'] = 'Ok';
        echo json_encode($response);
        exit;
    }

    public function addMembersEmailer()
    {
        // /** Email Setup */
        // $config = array(
        //     'protocol' => 'smtp',
        //     'smtp_host' => 'ssl://smtp.googlemail.com',
        //     'smtp_port' => 465,
        //     'smtp_user' => 'membership@dmk.in',
        //     'smtp_pass' => 'DMK$Mem20@)',
        //     'mailtype'  => 'html',
        //     'charset'   => 'utf-8'
        // );
        // $this->load->library('email', $config);
        // $this->email->set_newline("\r\n");
        // $mesg = $this->load->view('mail/membership', '', true);
        // // echo $mesg;exit;
        // $userErn = $this->Common_model->getSubscriber();
        // if (count($userErn) > 0) {
        //     $emails[] = 'pradip.nandwana@indianpac.com';
        //     foreach ($userErn as $users) {
        //         $updateUserArray[] = array('is_emailer_sent' => '1', 'id' => $users['id']);
        //         if ($users['email'] != '') {
        //             $emails[] = $users['email'];
        //         }
        //     }
        //     $this->db->update_batch('user_registrations', $updateUserArray, 'id');
        //     $this->email->from('membership@dmk.in', 'Goenchi Navi Sakal');
        //     $this->email->to('membership@dmk.in');
        //     $this->email->bcc($emails);
        //     $this->email->subject('Thank you so much for joining us!');
        //     $this->email->message($mesg);
        //     $this->email->send();
        // }

        echo json_encode(array('status' => 'success'));
        exit;
    }


    public function stalinAppEmailer()
    {
        // /** Email Setup */
        // $config = array(
        //     'protocol' => 'smtp',
        //     'smtp_host' => 'ssl://smtp.googlemail.com',
        //     'smtp_port' => 465,
        //     'smtp_user' => 'membership@dmk.in',
        //     'smtp_pass' => 'DMK$Mem20@)',
        //     'mailtype'  => 'html',
        //     'charset'   => 'utf-8'
        // );
        // $this->load->library('email', $config);
        // $this->email->set_newline("\r\n");
        // $mesg = $this->load->view('mail/stalinani', '', true);
        // // echo $mesg;
        // // exit;
        // $userErn = $this->Common_model->getStalinSubscriber();
        // if (count($userErn) > 0) {
        //     foreach ($userErn as $users) {
        //         $updateUserArray[] = array('stalin_emailer' => '1', 'id' => $users['id']);
        //         if ($users['email'] != '') {
        //             $email = $users['email'];
        //             // Set to, from, message, etc.
        //             $this->email->from('membership@dmk.in', 'Goenchi Navi Sakal');
        //             $this->email->to($email);
        //             $this->email->subject('An opportunity to meet with Mamata Banerjee');
        //             $this->email->message($mesg);
        //             $this->email->send();
        //         }
        //     }
        //     $this->db->update_batch('user_registrations', $updateUserArray, 'id');
        // }

        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function updateCounter()
    {
        $this->db->set('registrations', 'registrations+4663', FALSE);
        $this->db->set('today_registrations', 'today_registrations+4663', FALSE);
        $this->db->update('web_general_settings');
        $response['status'] = 'OK';
        echo json_encode($response);
        exit;
    }
}
