$(document).ready(function () {
  onlyNumber();
  onlyChar();
  var step = 1;
  $(".glc-card-div").hide();
  $("#phone_no").change(function () {
    var phone_no = $(this).val();
    if (phone_no != "") {
      $("#read_only_phone_no").val(phone_no);
      $("#otp_phone_no").text(phone_no);
    }
  });

  $("input[name=gender]:radio").click(function () {
    if ($("input[name=gender]:checked").val() == "Male") {
      var a = 'Yes';
      $('input[type=radio][name=femalehead][value='+ a +']').prop('disabled', true);
      $("#femaleheadNo").prop("checked", true);
      $(".headname").show();
      $(".headphonenumber").show();
    }
    else{
      var a = 'Yes';
      $('input[type=radio][name=femalehead][value='+ a +']').prop('disabled', false); 
    }
  });

  $("input[name=femalehead]:radio").click(function () {
    $(".headname").hide();
    $("#female_card_head").hide();
    $(".headphonenumber").hide();
    $("#femaleheadname").val("");
    $("#femalehead_phone_no").val("");
    if ($("input[name=femalehead]:checked").val() == "No") {
      $(".headname").show();
      $("#female_card_head").show();
      $(".headphonenumber").show();
    }
  });
  $("input[name=is_glc_card]:radio").click(function () {
    $(".glc-card-div").hide();
    $("#glc_cardno").val("");
    if ($("input[name=is_glc_card]:checked").val() == "Yes") {
      $(".glc-card-div").show();
    }
  });

  // Code for the Registration Validator
  var $regValidator = $("form[name='regForm']").validate({
    ignore: ":hidden",
    rules: {
      phone_no: {
        required: !0,
      },
    },
    messages: {
      phone_no: {
        required: lang.phone_no_req,
        maxlength: lang.phone_no_maxlength,
        minlength: lang.phone_no_minlength,
      },
    },
    errorPlacement: function (error, element) {
      if (element.is(":radio")) {
        error.appendTo(element.parents(".form-group"));
      } else if (element.is(":checkbox")) {
        error.appendTo(element.parents(".form-check"));
      } else {
        error.appendTo(element.parents(".form-group"));
      }
    },
  });
  jQuery.validator.addMethod("noSpace", function (value, element) {
    //Code used for blank space Validation
    return $.trim(value) != "";
  });

  $("#begin_btn").on("click", function (event) {
    event.preventDefault();
    var $valid = $("form[name='regForm']").valid();
    if (!$valid) {
      $regValidator.focusInvalid();
      return false;
    } else {
      ajaxindicatorstart("Please Wait..");
      $.ajax({
        url: lang.posturl+"welcome/sendOTP",
        type: "post",
        data: $("#regForm").serialize(),
        dataType: "json",
        success: function (data) {
          ajaxindicatorstop();
          if (data.status == "OK") {
            $(".step1").hide();
            $(".step2").show();
            step = 2;
            disableResend();
          } else {
            swal("Error", data.message, "error");
          }
        },
        error: function () {
          ajaxindicatorstop();
        },
      });
    }
  });

  $("#generateOTP").on("click", function () {
    var phone_no = $("#phone_no").val();
    if (phone_no.trim() != "") {
      ajaxindicatorstart("Please Wait..");
      $.ajax({
        url: "welcome/sendOTP",
        type: "post",
        data: $("#regForm").serialize(),
        dataType: "json",
        success: function (data) {
          ajaxindicatorstop();
          if (data.status == "OK") {
            disableResend();
          } else {
            swal("Error", data.message, "error");
          }
        },
        error: function () {
          ajaxindicatorstop();
        },
      });
    }
  });

  // Code for the Registration Validator
  var $otpValidator = $("form[name='regOTP']").validate({
    ignore: ":hidden",
    rules: {
      otp: {
        required: !0,
      },
    },
    messages: {
      otp: {
        required: lang.otp_req,
      },
    },
    errorPlacement: function (error, element) {
      if (element.is(":radio")) {
        error.appendTo(element.parents(".form-group"));
      } else if (element.is(":checkbox")) {
        error.appendTo(element.parents(".form-check"));
      } else {
        error.appendTo(element.parents(".form-group"));
      }
    },
  });

  $("#verify_btn").on("click", function (event) {
    event.preventDefault();
    var $valid = $("form[name='regOTP']").valid();
    if (!$valid) {
      $otpValidator.focusInvalid();
      return false;
    } else {
      ajaxindicatorstart("Please Wait..");
      var form = $("form[name='regOTP']")[0];
      var formData = new FormData(form);
      formData.append("phone_no", $("#phone_no").val());
      formData.append("referral_code", $("#referral_code").val());
      formData.append("language", $("#language").val());
      $.ajax({
        url: lang.posturl+"welcome/verifyOTP",
        type: "post",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (res) {
          data = JSON.parse(res);
          ajaxindicatorstop();
          if (data.status == "OK") {
            if (data.is_exist == true) {
              var guid = data.guid;
              if (data.retouch == true) {
                swal({
                  text: "Thank you for revisiting!",
                  type: "success",
                }).then(function () {
                  window.location.href =
                    lang.lang == "en"
                      ? "thankyou?uid=" + guid
                      : lang.lang == "kha"
                      ? "thankyou/kha?uid=" + guid
                      : "thankyou/garo?uid=" + guid;
                });
              } else {
                window.location.href =
                  lang.lang == "en"
                    ?lang.posturl+"thankyou?uid=" + guid
                    : lang.lang == "kha"
                    ?lang.posturl+"thankyou/kha?uid=" + guid
                    :lang.posturl+ "thankyou/garo?uid=" + guid;
              }
              return;
            }
            $(".step2").hide();
            $(".step3").show();
            step = 3;
          } else {
            swal("Error", data.message, "error");
          }
        },
        error: function () {
          ajaxindicatorstop();
        },
      });
    }
  });

  var $validator = $("form[name='surveyForm']").validate({
    ignore: ":hidden",
    rules: {
      name: {
        required: !0,
        noSpace: true,
      },
      read_only_phone_no: {
        required: !0,
      },
      father_name: {
        required: !0,
      },
      gender: {
        required: !0,
      },
      regMonth: {
        required: !0,
      },
      regDay: {
        required: !0,
      },
      regYear: {
        required: !0,
      },
      district: {
        required: !0,
      },
      ac: {
        required: !0,
      },
      age: {
        required: !0,
      },
      address: {
        required: !0,
      },
      // pincode: {
      //   required: !0,
      //   min: 403000,
      //   max: 403999,
      // },
      femalehead: {
        required: !0,
      },
      femaleheadname: {
        required: !0,
      },
      femalehead_phone_no: {
        required: !0,
      },
      // bankaccount: {
      //   required: !0,
      // },
      // village_name: {
      //   required: !0,
      // },
      confirmation: {
        required: !0,
      },
      glc_cardno: {
        required: !0,
      },
    },
    groups: {
      dateOfBirth: "regMonth regDay regYear",
    },
    messages: {
      name: {
        required: lang.full_name,
        noSpace: lang.full_name,
      },
      read_only_phone_no: {
        required: lang.phone_no_req,
        maxlength: lang.phone_no_maxlength,
        minlength: lang.phone_no_minlength,
      },
      father_name: {
        required: lang.father_husband_name,
      },
      gender: {
        required: lang.gender,
      },
      age: {
        required: lang.age,
      },
      regMonth: {
        required: lang.dob_req,
      },
      regDay: {
        required: lang.dob_req,
      },
      regYear: {
        required: lang.dob_req,
      },
      district: {
        required: lang.district,
      },
      ac: {
        required: lang.ac,
      },
      age: {
        required: lang.age,
      },
      address: {
        required: lang.address,
      },
      pincode: {
        required: lang.pincode,
        min: lang.goa_pin,
        max: lang.goa_pin,
      },
      femalehead: {
        required: lang.required_field,
      },
      femaleheadname: {
        required: lang.femaleheadname,
      },
      femalehead_phone_no: {
        required: lang.phone_no_req,
        maxlength: lang.phone_no_maxlength,
        minlength: lang.phone_no_minlength,
      },
      whatsapp_number: {
        maxlength: lang.whatsapp_no_maxlength,
        minlength: lang.whatsapp_no_minlength,
      },
      bankaccount: {
        required: lang.required_field,
      },
      village_name: {
        required: lang.village_name,
      },
      confirmation: {
        required: lang.required_field,
      },
      glc_cardno: {
        required: lang.glccard,
      },
    },
    errorPlacement: function (error, element) {
      if (element.is(":radio")) {
        error.appendTo(element.parents(".form-group"));
      } else if (element.is(":checkbox")) {
        error.appendTo(element.parents(".form-check"));
      } else if (
        element.attr("name") == "regMonth" ||
        element.attr("name") == "regDay" ||
        element.attr("name") == "regYear"
      ) {
        error.appendTo(element.parents(".form-group"));
      } else if (element.attr("name") == "dob") {
        error.appendTo(element.parents(".form-group"));
      } else {
        // This is the default behavior
        error.appendTo(element.parents(".form-group"));
        // error.insertAfter(element);
      }
    },
  });

  $(".selectpicker")
    .selectpicker()
    .change(function () {
      $(this).valid();
    });

  $(".btn-finish").click(function (event) {
    event.preventDefault();
    var $valid = $("form[name='surveyForm']").valid();
    if (!$valid) {
      $validator.focusInvalid();
      return false;
    } else {
      var form = $("form[name='surveyForm']")[0];
      var formData = new FormData(form);
      //formData.append("phone_no", $("#phone_no").val());
      // formData.append("otp", $("#otp").val());
      formData.append("language", $("#language").val());
      formData.append("ac_no", $("#ac").find(":selected").data("params"));
      formData.append("district_code", $("#district").find(":selected").data("params"));
      $.ajax({
        xhr: function () {
          var xhr = new window.XMLHttpRequest();
          xhr.upload.addEventListener(
            "progress",
            function (evt) {
              if (evt.lengthComputable) {
                var percentComplete = Math.round(
                  (evt.loaded / evt.total) * 100
                );
                $(".progress").show();
                $(".progress-bar").width(percentComplete + "%");
                var html = "";
                html +=
                  percentComplete == 100 ? "Submitted... " : "Submitting... ";
                $(".progress-bar").html(html + percentComplete + "%");
              }
            },
            false
          );
          return xhr;
        },
        url: lang.posturl+"welcome/registration",
        type: "post",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
          $(".progress-bar").width("0%");
          ajaxindicatorstart("Please Wait..");
        },
        success: function (res) {
          ajaxindicatorstop();
          data = JSON.parse(res);
          if (data.status == "OK") {
            window.location.href =
              lang.lang == "en"
                ? lang.posturl+"thankyou?uid=" + data.guid
                : lang.lang == "kha"
                ? lang.posturl+"thankyou/kha?uid=" + data.guid
                : lang.posturl+"thankyou/garo?uid=" + data.guid;
          } else {
            swal("Error", data.message, "error");
          }
        },
        error: function () {
          ajaxindicatorstop();
        },
      });
    }
  });

  $("input").keypress(function (event) {
    if (event.which == 13) {
      event.preventDefault();
      if (step == 1) {
        $("#begin_btn").click();
      } else if (step == 2) {
        $("#verify_btn").click();
      } else {
        $(".btn-finish").click();
      }
    }
  });
});

function onlyNumber() {
  $(".integer").on("input", function (event) {
    this.value = this.value.replace(/[^0-9]/g, "");
  });
}

function onlyChar() {
  $(".nameinput").on("input", function (event) {
    this.value = this.value.replace(/[^A-Za-z\s]/g, "");
  });
}

function disableResend() {
  $("#generateOTP").hide();
  timer(90);
  $(".timer_div").show();
}

let timerOn = true;

function timer(remaining) {
  // var m = Math.floor(remaining / 60);
  // var s = remaining;

  // m = m < 10 ? "0" + m : m;
  // s = s < 10 ? "0" + s : s;
  document.getElementById("timer").innerHTML = remaining;
  remaining -= 1;

  if (remaining >= 0 && timerOn) {
    setTimeout(function () {
      timer(remaining);
    }, 1000);
    return;
  }

  if (!timerOn) {
    // Do validate stuff here
    return;
  }
  $(".timer_div").hide();
  $("#generateOTP").show();
}

//loader
function ajaxindicatorstart(text) {
  $("#resultLoading").remove();
  if (jQuery(".content").find("#resultLoading").attr("id") != "resultLoading") {
    jQuery(".content").append(
      '<div id="resultLoading" style="display:none"><div><img src="'+lang.posturl+'assets/images/ajax-modal-loading.gif"><div>' +
        text +
        '</div></div><div class="bg"></div></div>'
    );
  }

  jQuery("#resultLoading").css({
    width: "100%",
    height: "100%",
    position: "fixed",
    "z-index": "10000000",
    top: "0",
    left: "0",
    right: "0",
    bottom: "0",
    margin: "auto",
  });

  jQuery("#resultLoading .bg").css({
    background: "#000000",
    opacity: "0.7",
    width: "100%",
    height: "100%",
    position: "absolute",
    top: "0",
  });

  jQuery("#resultLoading>div:first").css({
    width: "250px",
    height: "75px",
    "text-align": "center",
    position: "fixed",
    top: "0",
    left: "0",
    right: "0",
    bottom: "0",
    margin: "auto",
    "font-size": "16px",
    "z-index": "10",
    color: "#ffffff",
  });

  jQuery("#resultLoading .bg").height("100%");
  jQuery("#resultLoading").fadeIn("slow");
  jQuery(".container-contact100").css("cursor", "wait");
}

function ajaxindicatorstop() {
  jQuery("#resultLoading .bg").height("100%");
  jQuery("#resultLoading").fadeOut("slow");
  jQuery(".container-contact100").css("cursor", "default");
}
//end loader
